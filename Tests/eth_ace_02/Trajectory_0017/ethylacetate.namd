# QM/MM NAMD Configuration File

## Single QM region
structure      ethylacetate_proc.psf
coordinates    ethylacetate_proc.pdb
velocities     ethylacetate_proc.vel

## Output parameters
binaryoutput   no
binaryrestart  no
outputname     ethylacetate_out
dcdfile        ethylacetate_out.dcd
xstfile        ethylacetate_ou.xst
dcdfreq        1
restartfreq    1
xstFreq        1
restartname    ethylacetate_out.restart

## Thermostat Parameters
langevin 	           off

## Integrator Parameters
timestep 	            0.2
firstTimestep 	        0
fullElectFrequency      1
nonbondedfreq 	        1

# Force Field Parameters
paratypecharmm  on
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all22_prot.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all35_ethers.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36_carb.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36_cgenff.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36_lipid.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36_lipid_ljpme.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36m_prot.prm
parameters      /home/zronyj/Git/tccm_tfm/Molecules/toppar/par_all36_na.prm

exclude 	    scaled1-4
1-4scaling 	    1.0
rigidbonds 	    none
cutoff 		    12.0
pairlistdist 	14.0
switching 	    on
switchdist 	    10.0
margin          5

# Truns ON or OFF the QM calculations
qmForces        on

# Name of a secondary PDB file where the OCCupancy
# or BETA column has the indications for QM or MM atoms.
# QM atoms should have an integer bigger than zero (0) and
# MM atoms should have zero as the beta or occupancy field.
# The same file may have indications for bonds between a
# QM atom and an MM atom (if they exist).
qmParamPDB     "ethylacetate.pdb"

# Indicates qhich column has the QM/MM field.
# Column type may be "beta" or "occ"
qmColumn        "occ"

# Number of simultaneous QM simulations per node
QMSimsPerNode   1

# This will alter the point charges presented to the QM system. Available values
# are "none", "round" and "zero".
# NONE:  Nothing will be done. (DEFAULT)
# ROUND: This will change the most distant point charges so that the total 
#        sum of point charges is a whole number.
# ZERO: This will charne the most distant point charges so that the total 
#       sum of point charges is ZERO.
QMPointChargeScheme none

# Indicates what will be the treatment given to QM-MM bonds in terms of
# charge distribution and dummy atom creation and placement.
# CS: Charge Shift Scheme. (DEFAULT)
# RCD: Redistributed Charge and Ddipole method
# Z1: Only ignored MM1 partial charge, no charge distribution.
# Z2: Ignores MM1 and all MM2 partial charges.
# Z3: Ignores MM1 and all MM2 and MM3 partial charges.
QMBondScheme "Z3"

# Directory where QM calculations will be ran.
# This should be a fast read/write location, such as a RAM
# folder (/dev/shm on linux distros). You will need to create such folder.
qmBaseDir  "/dev/shm/0017"

# The string passed to "qmConfigLine" will be copied and pasted at the very
# begining of the configuration file for the chosen QM software.
qmConfigLine "! PBE def2-SV(P) EnGrad NoTrah"
qmConfigLine "%%maxcore 1536"
qmConfigLine "%%pal nprocs 4 end"
qmConfigLine "%%scf
maxiter 100
TolE 1e-1
ConvCheckMode 1
end"

# Multiplicity of the QM region. This is needed for propper 
# construction of ORCA's input file.
qmMult          "1 1"

# Indicates the charge of each QM region. If no charge is provided for a QM region,
# NAMD calculates the total charge automatically based on the given parameter set.
qmCharge        "1 0"

# Indicates which QM software should be used. Currently we only supoprt "mopac", "orca" and "custom".
qmSoftware      "orca"

# Path to the executable
qmExecPath      "/opt/ORCA_lin/orca"

# Number of steps in the QM/MM simulation.
run 500