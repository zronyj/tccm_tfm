

                     SUMMARY OF  PM6 CALCULATION

                                                       MOPAC v22.0.4 Linux
                                                       Sun Apr 16 04:30:44 2023

           Empirical Formula: C6 H9 N3 O2  =    20 atoms

 CHARGE=0 PM6 PRECISE XYZ T=2M MOZYME CUTOFF=9.0 AUX LET GRAD QMMM GEO-OK
 Geometry optimized using OpenBabel with MMFF94s



     GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING (EF).     
     SCF FIELD WAS ACHIEVED                                   

          HEAT OF FORMATION       =        -56.53267 KCAL/MOL =    -236.53270 KJ/MOL
          GRADIENT NORM           =          0.86764          =       0.19401 PER ATOM
          DIPOLE                  =          1.84949 DEBYE   POINT GROUP:  C1  
          NO. OF FILLED LEVELS    =         30
          MOLECULAR WEIGHT        =        155.1560

          MOLECULAR DIMENSIONS (Angstroms)

            Atom       Atom       Distance
            H    19    H    13     7.48325
            H    12    O    11     5.16502
            H    16    H    20     3.78821
          SCF CALCULATIONS        =         57
          WALL-CLOCK TIME         =      0.330 SECONDS
          COMPUTATION TIME        =      2.359 SECONDS


          FINAL GEOMETRY OBTAINED
 CHARGE=0 PM6 PRECISE XYZ T=2M MOZYME CUTOFF=9.0 AUX LET GRAD QMMM GEO-OK
 Geometry optimized using OpenBabel with MMFF94s

  C     0.16734594 +1  -0.38855974 +1   1.30868199 +1
  N    -0.90247595 +1   0.44967757 +1   0.99770166 +1
  C    -0.92040570 +1   0.56993458 +1  -0.34897309 +1
  N     0.12677196 +1  -0.17474728 +1  -0.92323291 +1
  C     0.81779866 +1  -0.79357362 +1   0.13657358 +1
  C     1.98700693 +1  -1.67924057 +1  -0.05743299 +1
  C     1.58233792 +1  -3.17123340 +1  -0.09560461 +1
  C     0.62580160 +1  -3.39164782 +1  -1.28309443 +1
  N     2.80229315 +1  -3.99891909 +1  -0.19526945 +1
  O    -0.71216800 +1  -3.18877574 +1  -1.04273847 +1
  O     0.93770719 +1  -3.72458789 +1  -2.39717955 +1
  H     0.40695179 +1  -0.63403786 +1   2.32875829 +1
  H    -1.62539658 +1   1.15151778 +1  -0.92566960 +1
  H     0.32426649 +1  -0.29389102 +1  -1.90179408 +1
  H     2.71434900 +1  -1.53675270 +1   0.77995889 +1
  H     2.56132099 +1  -1.40847657 +1  -0.96999756 +1
  H     1.05960914 +1  -3.43895230 +1   0.86335289 +1
  H     3.29085667 +1  -3.83885758 +1  -1.07501752 +1
  H     2.57858154 +1  -4.99013137 +1  -0.14739504 +1
  H    -0.93315486 +1  -2.81966436 +1  -0.15156149 +1
 
