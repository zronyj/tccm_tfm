#!/usr/bin/env python3

# ------------------ Importing libraries ------------------
import os
import sys
import math
import shutil
import numpy as np
import subprocess
from multiprocessing import Pool
from scipy import constants as cts

# Consider the following:
# ParmEd: https://parmed.github.io/ParmEd/html/index.html
# OpenBabel: https://openbabel.org/docs/current/UseTheLibrary/PythonDoc.html

# -------------------- Some  Constants --------------------

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

# ------------------- Define  functions -------------------

# ===> General

# Function to compute the center of mass and compute coordinates with respect to it
def center_of_mass(coords_v, masses):
    if coords_v.shape[1] != 1:
        raise ValueError("The coordinates should be shaped as an 3*N vector!")
    
    # Reshape the coordinates, and multiply each atom by its mass
    coords = coords_v.reshape(na, 3)
    mass_weighting = coords.T * masses

    # Sum all the x, y and z coordinates, and dividing them through the mass
    center_of_mass = mass_weighting.sum(axis=1)/masses.sum()
    
    # Re-calculating the coordiantes with respect to the COM
    new_coords = np.array([ c - center_of_mass for c in coords ])
    return center_of_mass, new_coords

# Function to compute the inertia tensor of a molecule
def inertia_tensor(coords_v, masses, na):
    if coords_v.shape[1] != 1:
        raise ValueError("The coordinates should be shaped as an 3*N vector!")

    # Reshape the coordinates
    coords = coords_v.reshape(na, 3)

    # Compute the diagonal elements
    Ixx = sum([ masses[i] * (coords[i][1]**2 + coords[i][2]**2) for i in range(na)])
    Iyy = sum([ masses[i] * (coords[i][0]**2 + coords[i][2]**2) for i in range(na)])
    Izz = sum([ masses[i] * (coords[i][0]**2 + coords[i][1]**2) for i in range(na)])

    # Compute the non-diagonal elements
    Ixy = sum([ masses[i] * coords[i][0] * coords[i][1] for i in range(na)])
    Iyz = sum([ masses[i] * coords[i][1] * coords[i][2] for i in range(na)])
    Ixz = sum([ masses[i] * coords[i][0] * coords[i][2] for i in range(na)])

    # The matrix is symmetrical!
    return np.array([[Ixx, -Ixy, -Ixz], [-Ixy, Iyy, -Iyz], [-Ixz, -Iyz, Izz]])

# Function to compute the kinetic energy of the atoms in the molecule
# Unit: J
def kinetic_energy(mass, velocity):
    return 0.5 * ((mass @ velocity).T @ velocity) * ( 1E-10 * 1E12 )**2 * 1E-3

# Function to compute the potential energy of the atoms in the molecule
# Unit: J
def potential_energy(frequencies, mass, displacement):
    sqrt_k = 2 * np.pi * 100 * cts.speed_of_light * np.array([0]*6 + [f for f in frequencies])
    sqrt_kx = np.sqrt(mass) @ np.diag(sqrt_k) @ displacement * 1E-10 * 1E-3
    return 0.5 * sqrt_kx.T @ sqrt_kx

# ===> Normal Modes treatment

# Function to extract the required data from the Orca output
def get_data(nombre, periodic=periodic):

    with open(nombre, 'r') as f:
        data = f.readlines()

    na_init = 0             # Line number for number of atoms
    coord_init = 0          # Line number for atom coordinates
    nm_init = 0             # Line number for normal modes
    freqs_init = 0          # Line number for frequencies
    # Check where are the lines of interest
    for ln in range(len(data)):
        # Find the line with the information on the number of atoms in the molecule
        if "Number of atoms" in data[ln] and not na_init:
            na_init = ln
        # Find the line where the cartesian coordinates of the molecule start
        if "> *xyz" in data[ln]:
            coord_init = ln + 1
        # Find the line where the frequencies of the molecule start
        if "VIBRATIONAL FREQUENCIES" in data[ln]:
            freqs_init = ln + 5
        # Find the line where the normal modes of the molecule start
        if "NORMAL MODES" in data[ln] and not nm_init:
            nm_init = ln + 7

    # Extract the number of atoms in the molecule
    na_line = data[na_init].split()
    na = int(na_line[-1])

    # Get the coordinates of all atoms
    coord_lines = data[coord_init: coord_init + na]
    coord_lines = [l.split() for l in coord_lines]
    coords = [[float(l[i]) if i > 2 else l[i] for i in range(2, len(l))] for l in coord_lines]

    # Get the masses for each atom individually
    masses = np.array([ periodic[c[0]] for c in coords ])
    mass = masses.sum()

    # Get the frequencies
    freqs_num = na * 3
    freqs_lines = data[freqs_init: freqs_init + freqs_num]
    freqs_lines = [l.split() for l in freqs_lines]
    freqs = np.array([float(l[1]) for l in freqs_lines if float(l[1]) != 0])

    # Get the normal modes
    # https://en.wikipedia.org/wiki/Normal_mode
    nm_num = na * 3
    nm_chunks = math.ceil(na / 2)
    nm_lines = []
    for c in range(nm_chunks):
        nm_lines += data[nm_init + c * (nm_num + 1): nm_init + (c + 1) * (nm_num + 1)]

    nm_lines = [l.split() for l in nm_lines]

    nm = [[] for r in range(nm_num)]

    for i in range(len(nm_lines)):
        ent = i%(nm_num + 1) - 1
        if ent != -1:
            nm[ent] += [float(n) for n in nm_lines[i][1:]]

    # Create the normal modes matrix using NumPy
    nm = np.array(nm)

    # Check that the normal modes don't include translations and rotations (first 5 or 6 normal modes)
    # https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/Vibrational_Spectroscopy/Vibrational_Modes/Introduction_to_Vibrations
    check = nm.sum(axis=0)
    if check[5] <= 1E-15:
        nm = nm[:,6:].T.reshape(3*na - 6, na, 3)
    else:
        nm = nm[:,5:].T.reshape(3*na - 5, na, 3)

    return na, mass, masses, coords, nm, freqs

# Function to compute the energy of the Harmonic Oscillator for a given frequency
def e_qharmonic(frequency, n):
    # Planck constant = 6.62607015e-34 Js^{-1}
    # E = h * v * (n + 0.5)
    return cts.Planck * frequency * ( n + 0.5 )

# Function to compute the ZPE of a quantum harmonic oscillator for each frequency
def get_ZPEs(vibs, n=0):
    # Consider that each frequency is given in cm^{-1}. It is, therefore,
    # changed to m^{-1} and multiplied by the speed of light to get it in s^{-1}
    zpes = [e_qharmonic(100 * v * cts.speed_of_light, n) for v in vibs if v != 0]
    return zpes

# Function to normalize a numpy array vector
def normalize(randarray):
    return randarray / np.abs(randarray.sum())

# Compute the force vectors using random assigned normal modes
def compute_vectors(coordinates, masses, frequencies, normal_modes, new_energy, seed=42):

    num_atoms = len(coordinates)
    num_nms = len(normal_modes)

    # Mass matrix
    mass_diag = [0]*num_atoms*3
    for k in range(num_atoms):
        mass_diag[3*k] = masses[k]
        mass_diag[3*k+1] = masses[k]
        mass_diag[3*k+2] = masses[k]
    M = np.diag(mass_diag)
    iM = np.diag([1/masa if masa != 0 else 0 for masa in mass_diag])

    # The energy contribution by kinetic (T) or potential (V) will be devided by this
    # random number
    np.random.seed(seed)
    R = np.random.random()

    new_energies = [0]*num_nms
    amplitudes = [0]*num_nms
    q_positns = [0]*num_nms
    q_velocts = [0]*num_nms

    for i in range(num_nms):

        # Frequencies
        # Consider that each frequency is given in cm^{-1}
        # s^{-1} = 1 / cm * [ 100 cm / 1 m ] * [ 3E8 m / s ] 
        # Unit: 1 / s
        v = 100 * frequencies[i] * cts.speed_of_light

        # Compute the new energies by following the equation
        # E[i] = (E[0][i] - sum(E[:i])) * (1 - R^(1/(n - i)))
        # Unit: J
        temp_energy = (new_energy - sum(new_energies[:i])) * (1 - np.random.random()**(1/(num_nms - i)))

        # Find the harmonic oscillator level that corresponds to this energy
        level = round( temp_energy / ( cts.Planck * v ) )
        if level < 0: level = 0

        # Compute the real energy at the found harmonic oscillator level
        new_energies[i] = e_qharmonic(v, level)

        # Compute the amplitude
        # w = omega = 2 * pi * v
        # E = 1/2 * k * (x - x0)^2
        # x - x0 = (2 * E / k)^0.5              |  k = omega^2 * m = 4 * pi^2 * v^2 * m
        # x - x0 = (2 * E / m)^0.5 / (2 * pi * v) = A
        # { This lacks the one over sqrt of mass component!!! }
        # Unit: sqrt(kg) * m
        A_numerator = np.sqrt(2 * new_energies[i])
        omega = 2 * np.pi * v
        amplitudes[i] = A_numerator / omega

        # Compute the new position and momentum
        #            E     = T + V = p^2 / 2m + (k * q^2) / 2
        #                  = 1/2 * m * v^2 + 1/2 * k * q^2
        #                  = 1/2 * m * (q')^2 + 1/2 * k * q^2
        #            2 * E = m * (q')^2 + k * q^2
        #            1     = m/2E * (q')^2 + k/2E * q^2
        # Thinking of an ellipse, q can be found as a trigonometric function (of a variable u)
        # When q = 0, the potential energy should be 0 and kinetic should be maximum
        # When q = amplitude, the potential energy should be maximum and the kinetic 0
        # Therefore,
        #            q (u) = (2E/k)^0.5 * sin((k/m)^0.5 * u)
        #                  = (2E/m)^0.5 / (2 * pi * v) * sin( 2 * pi * v * u )
        #                  = A * sin( 2 * pi * v * u )
        #            q'(u) = (2E/k)^0.5 * (k/m)^0.5 * cos( 2 * pi * v * u )
        #                  = (2E/m)^0.5 * cos( 2 * pi * v * u )
        # Which further leads to
        #            q = A * sin( 2 * pi * v * u )
        #            p = (2 * m * E)^0.5 * cos( 2 * pi * v * u )
        # The maximum value that the energy can achieve is when
        #     a) V = 0 and T = max
        #     b) T = 0 and V = max
        # Proceeding with (b) ...
        #            (2E/m)^0.5 * cos( 2 * pi * v * u ) = 0
        #            2 * pi * v * u = cos^(-1)(0) = (2*n + 1) * pi / 2  | n = 0, 1, 2, ...
        #            u = (2*n + 1) / (4 * v)
        # Considering that the periodicity will lead to equal values, the maximum
        # can be obtained by evaluating n = 0
        #            u_{max} = 1 / (4 * v)
        # Now, considering that any random number between 0 and the maximum u is a valid
        # coordinate, then a uniform distribution random number R can be chosen as:
        #            R * 1 / (4 * v)
        # Substituting into the position and velocity equations ...
        #            q (u) = (2E/m)^0.5 / (2 * pi * v) * sin( pi/2 * R )
        #            q'(u) = (2E/m)^0.5 * cos( pi/2 * R )
        # Units: sqrt(kg) * m
        q_positns[i] = amplitudes[i] * np.sin( R * np.pi / 2 )
        # Units: sqrt(kg) * m / s
        q_velocts[i] = A_numerator * np.cos( R * np.pi / 2 )

    # Compute the new energy according to the normal modes
    new_nm_energy = sum(new_energies)

    # Equilibrium geometry coordinates
    # Unit: Angstrom
    x0 = np.array([ a[1:] for a in coordinates ]).reshape(num_atoms*3,1)

    # Mass-weighted cartesian displacements
    # 1/sqrt(g/mol) = sqrt(mol/g)
    # Unit: sqrt(mol/g)
    L = normal_modes.reshape(num_nms, num_atoms*3).T

    # Cartesian displacements
    # sqrt(g/mol) * sqrt(mol/g) = nothing
    # Unit: none!
    Lx = np.sqrt(M) @ L

    # Normal coordinates
    # sqrt(J)/(1/s) = sqrt( kg m^2 s^-2 ) / (1/s) = sqrt(kg) m
    # 1 m = 10^10 Angstrom
    # 1 g = n kg * [1000 g / 1 kg ]
    # Unit: sqrt(g) Angstrom
    # {The sqrt(g) part comnes from the lack of mass in the amplitude}
    Q = np.array(q_positns).reshape(num_nms,1) * np.sqrt(1000) * 1E10

    # Normal velocities
    # sqrt(J)/(1/s) = sqrt( kg m^2 s^-2 ) = sqrt(kg) m/s
    # 1 m = 10^10 Angstrom
    # 1 g = n kg * [1000 g / 1 kg ]
    # 1 s = 10^12 ps
    # Unit: sqrt(g) Angstrom / ps
    # {The sqrt(g) part comnes from the lack of mass in the amplitude}
    Qp = np.array(q_velocts).reshape(num_nms,1) * np.sqrt(1000) * 1E-2

    # Displacement
    # sqrt(mol/g) * sqrt(g) * Angstrom = Angstrom
    # {Fixing the lack of mass in the amplitude}
    # Unit: Angstrom
    x = (np.sqrt(iM) @ Lx) @ Q

    # Velocity
    # sqrt(mol/g) * sqrt(g) * Angstrom / ps = Angstrom / ps
    # {Fixing the lack of mass in the amplitude}
    # Unit: Angstrom / ps
    v = (np.sqrt(iM) @ Lx) @ Qp

    # Compute the current kinetic energy
    Kinetic = kinetic_energy(M, v)

    # Compute the current potential energy
    Potential = potential_energy(frequencies, M, x)

    # Compute the current total energy of the molecule
    E_tentative = Kinetic + Potential

    # Estimate the deviation of the energy from the target energy
    scaling_factor = np.sqrt(new_nm_energy/E_tentative)

    # Scale both the positions and the velocities
    x = x * scaling_factor * np.sqrt(cts.Avogadro)
    v = v * scaling_factor * np.sqrt(cts.Avogadro)

    # Final coordinates and velocities
    new_x = (x + x0).reshape(num_atoms, 3)
    new_v = v.reshape(num_atoms, 3)

    final_coords = []
    final_velocs = []
    for i in range(num_atoms):
        final_coords.append([ coordinates[i][0], new_x[i][0], new_x[i][1], new_x[i][2] ])
        final_velocs.append([ coordinates[i][0], new_v[i][0], new_v[i][1], new_v[i][2] ])

    return final_coords, final_velocs, new_nm_energy

# ===> Molecular Dynamics treatment

# Create a PDB file from the original XYZ file
def get_pdb_data(xyz_file_name):
    # Convert the XYZ coordinates into a PDB file using Open Babel
    # https://open-babel.readthedocs.io/en/latest/Command-line_tools/babel.html
    temp_pdb = subprocess.run(['obabel', '-i', 'xyz', f"{xyz_file_name}.xyz", '-o', 'pdb', '-O', f"temp_{xyz_file_name}.pdb"])
    with open(f"temp_{xyz_file_name}.pdb", 'r') as old_pdb:
        pdb_data = old_pdb.readlines()
        for i, l in enumerate(pdb_data):
            if (('ATOM' in l) or ('HETATM' in l)):
                old_line = l.split()
                new_line = [ f"{old_line[0]:<6}", f"{old_line[1]:>4}",
                f" {old_line[2]:<3}", " QM", "A", "  1    ",  "{:>7.3f}",
                "{:>7.3f}", "{:>7.3f}", " 1.00", " 0.00", f"{old_line[-1]:>11}  "]
                pdb_data[i] = " ".join(new_line)
            if pdb_data[i][-1] != "\n":
                pdb_data[i] = pdb_data[i] + "\n"
        pdb_template = "".join(pdb_data)
    os.remove(f"temp_{xyz_file_name}.pdb")
    return pdb_template

# Create a nonsense parameter file for NAMD
def make_rtf(atoms):
    lines = []
    lines.append("\n\nread rtf card append\n\nRESI QM         0.00\n\n")
    for atom in atoms:
        lines.append(f"ATOM {atom[0]}    {atom[0]}      0.00\n")
    lines.append("end\n\n")
    with open("QM.rtf", "w") as f:
        f.writelines(lines)

def make_psf(na, coords, nname, periodic=periodic):

    lines = ["PSF\n\n       1 !NTITLE\n REMARKS VMD-generated NAMD/X-Plor PSF structure file\n\n"]

    lines.append(f"{na:8} !NATOM\n")

    for i, a in enumerate(coords):
        lines.append(f"{i + 1:8} U    1    QM  {a[0]}    {a[0]}      0.000000  {periodic[a[0]]:14.4F}           0\n")

    lines.append("\n       0 !NBOND: bonds\n\n\n")
    lines.append("       0 !NTHETA: angles\n\n\n")
    lines.append("       0 !NPHI: dihedrals\n\n\n")
    lines.append("       0 !NIMPHI: impropers\n\n\n")
    lines.append("       0 !NDON: donors\n\n\n")
    lines.append("       0 !NACC: acceptors\n\n\n")
    lines.append("       0 !NNB\n\n\n")
    lines.append("       1       0 !NGRP\n\n")

    with open(nname, "w") as psf:
        psf.writelines(lines)

# Write a coordinates or velocities file
def make_pdb(nname, base, coords):
    with open(base, "r") as f:
        data = f.readlines()

    init, final = 0, 0
    for l in range(len(data)):
        if ("ATOM" in data[l]) and not("ATOM" in data[l-1]) and (init == 0):
            init = l
        if ("ATOM" in data[l]) and not("ATOM" in data[l+1]) and (final == 0):
            final = l

    if final - init == 0:
        for l in range(len(data)):
            if ("HETATM" in data[l]) and not("HETATM" in data[l-1]) and (init == 0):
                init = l
            if ("HETATM" in data[l]) and not("HETATM" in data[l+1]) and (final == 0):
                final = l

    num_atoms = len(data[init: final+1])
    if num_atoms != len(coords):
        print(data[init: final+1])
        raise IndexError(f"The initial file and the given coordinates have a different amount of atoms!\n{num_atoms} and {len(coords)} respectively.")

    lines = []
    if nname[-3:] == "pdb":
        lines.append("CRYST1    0.000    0.000    0.000  90.00  90.00  90.00 P 1           1\n")
    else:
        lines.append("REMARK  RESTART VELOCITIES WRITTEN BY RONY FOR NAMD\n")
    for l in range(num_atoms):
        temp = data[init + l].split()
        lines.append(f"ATOM{int(temp[1]):7}  {temp[-1]}    QM X   1{coords[l][-3]:12.3F}{coords[l][-2]:8.3F}{coords[l][-1]:8.3F}  1.00  0.00      U    {temp[-1]}\n")
    lines.append("END\n")

    with open(nname, "w") as g:
        g.writelines(lines)

def run_NAMD(path_nam):
    nam = os.path.basename(path_nam)                # Get the name of the run
    folder = os.path.dirname(path_nam)              # Get the name/number of the trajectory 
    cwd = os.path.dirname(folder)                   # Get the name of the WD
    ramdir = os.path.join("/dev/shm", folder[-4:])  # Get the name of the RAM directory
    print("-"*70 + "> " + os.path.basename(folder)) # Show me what trajectory are you working in
    if os.path.exists(ramdir):                      # If the RAM directory exists, delete it!
        shutil.rmtree(ramdir)
    os.mkdir(ramdir)                                # Create the RAM directory
    os.chdir(folder)
    with open(f"{nam}.log", "w") as f:              # Open a log file
        # Run NAMD!
        log = subprocess.run(["/opt/NAMD2mc/namd2", "+p2", f"{nam}.namd"], stdout=f)
    os.chdir(cwd)                                   # Get out of the trajectory's folder
    shutil.rmtree(ramdir)                           # Delete the RAM directory

# ----------------- Initialize  variables -----------------

target_energy = 300      # Set the initial total energy of the molecule in kcal/mol
trajectories = 20        # Set the number of trajectories
simulation_name = "test" # The name of the files

# ------------------------ Program ------------------------

if __name__ == "__main__":

    # np.seterr('raise')

    # Check whether the command was run with a file as argument
    if len(sys.argv) != 2:
        raise ValueError("No file was provided to parse the normal modes.")

    # Check whether the provided file has the correct extension
    if not (".out" in sys.argv[1]):
        raise ValueError("The provided file is not an Orca output file.")

    # Setting the random numbers for deterministic results
    random_seed = np.random.randint(1E6, 1E7)
    print(f"Random seed for this run is: {random_seed}")
    np.random.seed(random_seed)

    # Get all the information from the ORCA output file
    na, mass, masses, coords, nm, freqs = get_data(sys.argv[1])

    # ===========================================================

    # Compute the ZPE of the molecule
    zpes = get_ZPEs(freqs)
    zpe = sum(zpes)

    Etot = target_energy * (1000 * cts.calorie) / cts.Avogadro

    print(f"The Zero Point Energy for this molecule is {zpe:.4E} J or {zpe * cts.Avogadro / (1000 * cts.calorie):.4F} kcal/mol.")
    print(f"According to the provided energy, the new system energy should be around {Etot:.4E} J. or {target_energy:.4F} kcal/mol.")

    #new_coords, vels, new_Etot = compute_vectors(coords, masses, freqs, nm, Etot)

    # Generate random numbers for the trajectories
    seed_field = np.random.randint(1000000, 9999999, size=trajectories)

    # Getting variables ready for all the data
    new_coords = [0]*trajectories
    vels = [0]*trajectories
    new_Etot = [0]*trajectories

    # # Doing the process over all available cores
    with Pool() as p:
        # Put all arguments in the same item for each set of random numbers
        items = [(coords, masses, freqs, nm, Etot, rn) for rn in seed_field]
        # Compute the coordinates, velocity vectors and new energies
        everything = p.starmap(compute_vectors, items)

    new_coords, vels, new_Etot = zip(*everything)

    print(f"But the average system energy will be around {sum(new_Etot)/trajectories:.4E} J. or {sum(new_Etot)/trajectories * cts.Avogadro / (1000 * cts.calorie):.4F} kcal/mol.")

    # ===========================================================

    # Get the data structure for future PDB files
    pdb_template = get_pdb_data("test")

    # Create the NAMD configuration file to run the simulation
    # Use EnGrad: https://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2016-2017/1376.html
    # Where is the temp file? /dev/shm - https://www.cyberciti.biz/tips/what-is-devshm-and-its-practical-usage.html
    with open('template.namd', 'r') as namd_conf_template:
        template = namd_conf_template.read()

    # Get the current location
    here = os.getcwd()

    # Create all the input files for all trajectories in separate folders
    for traj in range(trajectories):

        # Establish new trajectory's path
        new_dir = os.path.join(here, f"Trajectory_{traj:04d}")

        # If the directory exists, delete it
        if os.path.exists(new_dir):
            shutil.rmtree(new_dir)

        # Create the directory
        os.mkdir(new_dir)

        # Go into the new folder
        os.chdir(new_dir)

        # Temporary RAM folder
        tmppath = os.path.join("/dev/shm", f"{traj:04d}")

        # Save the XYZ coordinates
        # https://open-babel.readthedocs.io/en/latest/FileFormats/XYZ_cartesian_coordinates_format.html
        lines = []
        lines.append(f"{na}\n")
        lines.append("Molecule in XYZ format generated with MS\n")
        lines += [" {}\t{:>18.15f}\t{:>18.15f}\t{:>18.15f}\n".format(*a) for a in new_coords[traj]]

        with open(f"{simulation_name}.xyz", "w") as xyz:
            xyz.writelines(lines)

        # Create the PDB file with the new coordinates for this run
        temp_coords_only = [ atom[1:] for atom in new_coords[traj] ]
        temp_coords = []
        for tco in temp_coords_only:
            temp_coords += tco

        with open(f"{simulation_name}.pdb", "w") as pdb:
            pdb.write(pdb_template.format(*temp_coords))

        # Create a topology file with "something"
        make_rtf(new_coords[traj])

        # Create the new PDB with the initial coordinates for the MD
        make_pdb(f"{simulation_name}_proc.pdb", f"{simulation_name}.pdb", new_coords[traj])

        # Ask VMD to create a PSF file based on the latter
        make_psf(na, new_coords[traj], f"{simulation_name}_proc.psf")

        # Fill in NAMD template
        namd_file_content = template.format(name=f"{simulation_name}", tmpfolder=tmppath)

        with open(f"{simulation_name}.namd", "w") as namd_conf:
            namd_conf.write(namd_file_content)

        # Create the new velocities file
        make_pdb(f"{simulation_name}_proc.vel", f"{simulation_name}.pdb", vels[traj])

        # Get out of the folder
        os.chdir(here)

    # ===========================================================

    # Running the QM/MD simulation
    # Running over 2 cores
    with Pool(2) as p:

        # Compute the name of all the folders
        trajs = [os.path.join(os.path.join(here, f"Trajectory_{t:04d}"), simulation_name) for t in range(trajectories)]

        # Run QM/MD with NAMD
        jobs = p.map(run_NAMD, trajs)