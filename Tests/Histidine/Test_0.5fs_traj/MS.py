# ------------------ Importing libraries ------------------
import os
import sys
import math
import shutil
import numpy as np
import subprocess
from multiprocessing import Pool
from scipy import constants as cts

# Consider the following:
# ParmEd: https://parmed.github.io/ParmEd/html/index.html
# OpenBabel: https://openbabel.org/docs/current/UseTheLibrary/PythonDoc.html

# ------------------- Define  functions -------------------

# ===> Normal Modes treatment

# Function to compute the energy of the Harmonic Oscillator for a given frequency
def e_qharmonic(omega, n):
    # Planck constant = 6.62607015e-34 Js^{-1}
    return cts.Planck * omega * ( n + 0.5 )

# Function to compute the ZPE of a quantum harmonic oscillator for each frequency
def get_ZPE(vibs):
    # Consider that each frequency is given in cm^{-1}. It is, therefore,
    # changed to m^{-1} and multiplied by the speed of light to get it in s^{-1}
    zpes = [e_qharmonic(100 * v * cts.speed_of_light, 0) for v in vibs if v != 0]
    return sum(zpes)

# Function to normalize a numpy array vector
def normalize(randarray):
    return randarray / np.abs(randarray.sum())

# Function to extract the required data from the Orca output
def get_data(data):
    na_init = 0             # Line number for number of atoms
    mol_mass = 0            # Line number for the mass of the molecule
    coord_init = 0          # Line number for atom coordinates
    nm_init = 0             # Line number for normal modes
    freqs_init = 0          # Line number for frequencies
    # Check where are the lines of interest
    for ln in range(len(data)):
        # Find the line with the information on the number of atoms in the molecule
        if "Number of atoms" in data[ln] and not na_init:
            na_init = ln
        # Find the line with the molecular mass
        if "Total Mass" in data[ln] and not mol_mass:
            mol_mass = ln
        # Find the line where the cartesian coordinates of the molecule start
        if "CARTESIAN COORDINATES (ANGSTROEM)" in data[ln]:
            coord_init = ln + 2
        # Find the line where the frequencies of the molecule start
        if "VIBRATIONAL FREQUENCIES" in data[ln]:
            freqs_init = ln + 5
        # Find the line where the normal modes of the molecule start
        if "NORMAL MODES" in data[ln] and not nm_init:
            nm_init = ln + 7

    # Extract the number of atoms in the molecule
    na_line = data[na_init].split()
    na = int(na_line[-1])

    # Extract the mass of the moleculee
    mass_line = data[mol_mass].split()
    mass = float(mass_line[3])

    # Get the coordinates of all atoms
    coord_lines = data[coord_init: coord_init + na]
    coord_lines = [l.split() for l in coord_lines]
    coords = [[float(l[i]) if i > 0 else l[i] for i in range(len(l))] for l in coord_lines]

    # Get the frequencies
    freqs_num = na * 3
    freqs_lines = data[freqs_init: freqs_init + freqs_num]
    freqs_lines = [l.split() for l in freqs_lines]
    freqs = [float(l[1]) for l in freqs_lines]

    # Get the normal modes
    # https://en.wikipedia.org/wiki/Normal_mode
    nm_num = na * 3
    nm_chunks = math.ceil(na / 2)
    nm_lines = []
    for c in range(nm_chunks):
        nm_lines += data[nm_init + c * (nm_num + 1): nm_init + (c + 1) * (nm_num + 1)]

    nm_lines = [l.split() for l in nm_lines]

    nm = [[] for r in range(nm_num)]

    for i in range(len(nm_lines)):
        ent = i%(nm_num + 1) - 1
        if ent != -1:
            nm[ent] += [float(n) for n in nm_lines[i][1:]]

    # Create the normal modes matrix using NumPy
    nm = np.array(nm)
    return na, mass, coords, nm, freqs

# Check that the normal modes don't include useless data
# https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/Vibrational_Spectroscopy/Vibrational_Modes/Introduction_to_Vibrations
def remove_m56(normal_modes):
    check = normal_modes.sum(axis=0)
    if check[5] <= 1E-15:
        return normal_modes[:,5:]
    else:
        return normal_modes[:,4:]

# Compute the force vectors using random assigned normal modes
def compute_vectors(factors, na, normal_modes):
    sum_nms = np.matmul(normal_modes, factors)
    new_force_vects = sum_nms.reshape(na, 3)
    return new_force_vects

# ===> Molecular Dynamics treatment

# Replace occurrances of word in a file
def replace_residue(nombre, resbad, resgood):
    with open(nombre, "r") as f:
        data = f.read()
    os.remove(nombre)
    data = data.replace(resbad, resgood)
    with open(nombre, "w") as g:
        g.write(data)

# Create a nonsense parameter file for NAMD
def make_rtf(atoms):
    lines = []
    lines.append("\n\nread rtf card append\n\nRESI QM         0.00\n\n")
    for atom in atoms:
        lines.append(f"ATOM {atom[0]}    {atom[0]}      0.00\n")
    lines.append("end\n\n")
    with open("QM.rtf", "w") as f:
        f.writelines(lines)

# Generate a PSF and a PDB files to use in the MD
def get_psf(nombre):
    # Tcl reference: https://www.tcl.tk/man/tcl8.3/TclCmd/contents.html
    # psfgen guide: https://www.ks.uiuc.edu/Research/vmd/plugins/psfgen/ug.pdf
    # Make a PSF: https://www.ks.uiuc.edu/Training/Tutorials/namd/namd-tutorial-html/node6.html
    # PSF through QuikMD: https://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2019-2020/0402.html
    # TopoTools: https://sites.google.com/site/akohlmey/software/topotools?pli=1

    # Template of the Tcl script to be used in VMD [[ This approach is NOT working!]]
    instructs = """package require psfgen
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_cgenff.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all22_prot.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_carb.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_lipid.rtf
topology ./QM.rtf
mol new {nam}.pdb
set sel [atomselect top "all"]
$sel set segname U
pdbalias residue HIS HSE   
pdbalias residue HID HSD   
pdbalias residue HIP HSP
set residuos [$sel get resname]
set residuo [lindex $residuos 0]
foreach unidad [$sel get name] {{
    set lastchar [string index $unidad [expr [string length $unidad] - 1]]
    set eval [string is integer $lastchar]
    if {{ $eval == 1 }} then {{
        set newname [string range $unidad 0 [expr [string length $unidad] - 2]]
        pdbalias atom $residuo $unidad $newname
    }}
}}
segment U {{ pdb {nam}.pdb }}
coordpdb {nam}.pdb U
guesscoord
writepdb {nam}_proc.pdb
writepsf {nam}_proc.psf
quit
""".format(nam=nombre)    # Replace "nam" with the name of the molecule being used

    instructs = """package require topotools
mol new {nam}.pdb
mol reanalyze top
mol bondsrecalc top
set sel [atomselect top "all"]
$sel set segname U
topo guessbonds
topo guessangles
topo guessdihedrals
topo retypebonds
animate write psf {nam}_proc.psf
quit""".format(nam=nombre)

    # Save the Tcl script
    with open(f"{nombre}_psf_gen", "w") as psf_script:
        psf_script.write(instructs)

    # Run the Tcl script in VMD using text mode
    # VMD in text mode: https://www.ks.uiuc.edu/Training/Tutorials/vmd/tutorial-html/node8.html
    Xproc = subprocess.check_output(['vmd', '-dispdev', 'text', '-e', f"{nombre}_psf_gen"])
    os.remove(f"{nombre}_psf_gen")

def make_psf(nname, pdb):

    periodic = {"H": 1.0079,
    "Li": 6.9410,
    "Be": 9.0122,
    "B": 10.811,
    "C": 12.011,
    "N": 14.0067,
    "O": 15.9994,
    "F": 18.9984,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

    with open(pdb, "r") as f:
        data = f.readlines()

    init, final = 0, 0
    for l in range(len(data)):
        if ("ATOM" in data[l]) and not("ATOM" in data[l-1]) and (init == 0):
            init = l
        if ("ATOM" in data[l]) and not("ATOM" in data[l+1]) and (final == 0):
            final = l

    if final - init == 0:
        for l in range(len(data)):
            if ("HETATM" in data[l]) and not("HETATM" in data[l-1]) and (init == 0):
                init = l
            if ("HETATM" in data[l]) and not("HETATM" in data[l+1]) and (final == 0):
                final = l

    if final - init == 0:
        raise ValueError("Something is very wrong with the PDB file.")

    with open(nname, "r") as psf:
        almost = psf.readlines()

    atoms, bonds = 0, 0
    for l in range(len(almost)):
        if ("!NATOM" in almost[l]) and (atoms == 0):
            atoms = l
        if ("!NBOND" in almost[l]) and (bonds == 0):
            bonds = l

    lines = almost[:atoms+1]

    for l in data[init: final+1]:
        temp = l.split()
        lines.append(f"{int(temp[1]):8} U    1    QM  {temp[-1]}    {temp[-1]}      0.000000  {periodic[temp[-1]]:14.4F}           0\n")

    #lines += almost[bonds:]
    lines.append("\n       0 !NBOND: bonds\n\n\n")
    lines.append("       0 !NTHETA: angles\n\n\n")
    lines.append("       0 !NPHI: dihedrals\n\n\n")
    lines.append("       0 !NIMPHI: impropers\n\n\n")
    lines.append("       0 !NDON: donors\n\n\n")
    lines.append("       0 !NACC: acceptors\n\n\n")
    lines.append("       0 !NNB\n\n\n")
    lines.append("       1       0 !NGRP\n\n")

    with open(nname, "w") as psf:
        psf.writelines(lines)


def get_vecs(data):
    content = data.split("\n")
    d = []
    for l in content:
        if "cell" in l:
            d.append(l)
    return d

def get_cell(nombre):
    xtr = """mol new {nam}_proc.psf
mol addfile {nam}_proc.pdb
set all [atomselect top all] 
set minmax [measure minmax $all] 
set vec [vecsub [lindex $minmax 1] [lindex $minmax 0]] 
puts "cellBasisVector1 [lindex $vec 0] 0 0" 
puts "cellBasisVector2 0 [lindex $vec 1] 0" 
puts "cellBasisVector3 0 0 [lindex $vec 2]" 
set center [measure center $all] 
puts "cellOrigin $center" 
$all delete
mol delete top
quit""".format(nam=nombre)
    with open(f"{nombre}_vmd_extract", "w") as extractor:
        extractor.write(xtr)
    vec_data = subprocess.check_output(["vmd","-dispdev","text","-e", f"{nombre}_vmd_extract"])
    os.remove(nombre + "_vmd_extract")
    vecs = get_vecs(vec_data.decode('utf8'))
    return vecs

# Write a coordinates or velocities file
def make_pdb(nname, base, coords):
    with open(base, "r") as f:
        data = f.readlines()

    init, final = 0, 0
    for l in range(len(data)):
        if ("ATOM" in data[l]) and not("ATOM" in data[l-1]) and (init == 0):
            init = l
        if ("ATOM" in data[l]) and not("ATOM" in data[l+1]) and (final == 0):
            final = l

    if final - init == 0:
        for l in range(len(data)):
            if ("HETATM" in data[l]) and not("HETATM" in data[l-1]) and (init == 0):
                init = l
            if ("HETATM" in data[l]) and not("HETATM" in data[l+1]) and (final == 0):
                final = l

    num_atoms = len(data[init: final+1])
    if num_atoms != len(coords):
        raise IndexError(f"The initial file and the given coordinates have a different amount of atoms!\n{num_atoms} and {len(coords)} respectively.")

    lines = []
    if nname[-3:] == "pdb":
        lines.append("CRYST1    0.000    0.000    0.000  90.00  90.00  90.00 P 1           1\n")
    else:
        lines.append("REMARK  RESTART VELOCITIES WRITTEN BY RONY FOR NAMD\n")
    for l in range(num_atoms):
        temp = data[init + l].split()
        lines.append(f"ATOM{int(temp[1]):7}  {temp[-1]}    QM X   1{coords[l][-3]:12.3F}{coords[l][-2]:8.3F}{coords[l][-1]:8.3F}  1.00  0.00      U    {temp[-1]}\n")
    lines.append("END\n")

    with open(nname, "w") as g:
        g.writelines(lines)

def run_NAMD(path_nam):
    nam = os.path.basename(path_nam)                # Get the name of the run
    folder = os.path.dirname(path_nam)              # Get the name/number of the trajectory 
    cwd = os.path.dirname(folder)                   # Get the name of the WD
    ramdir = os.path.join("/dev/shm", folder[-4:])  # Get the name of the RAM directory
    print("-"*70 + "> " + os.path.basename(folder)) # Show me what trajectory are you working in
    if os.path.exists(ramdir):                      # If the RAM directory exists, delete it!
        shutil.rmtree(ramdir)
    os.mkdir(ramdir)                                # Create the RAM directory
    os.chdir(folder)
    with open(f"{nam}.log", "w") as f:              # Open a log file
        # Run NAMD!
        log = subprocess.run(["/opt/NAMD2mc/namd2", "+p2", f"{nam}.namd"], stdout=f)
    os.chdir(cwd)                                   # Get out of the trajectory's folder
    shutil.rmtree(ramdir)                           # Delete the RAM directory

# ----------------- Initialize  variables -----------------

trajectories = 500       # Set the number of trajectories
nm_factor = 25           # Factor to multiply the random Normal Mode as velocity
np.random.seed(42)       # Reset the random number generator
from_mps_to_Apps = 100   # Factor to convert meters per second to Angstrom per picosecond
simulation_name = "test" # The name of the files

# ------------------------ Program ------------------------

if __name__ == "__main__":

    # Check whether the command was run with a file as argument
    if len(sys.argv) != 2:
        raise ValueError("No file was provided to parse the normal modes.")

    # Check whether the provided file has the correct extension
    if not (".out" in sys.argv[1]):
        raise ValueError("The provided file is not an Orca output file.")

    # Open the Orca output file and read it
    with open(sys.argv[1], "r") as f:
        data = f.readlines()

    # Get the atom number, coordinates, normal modes and frequencies
    na, mass, coords, nm, freqs = get_data(data)

    # Remove normal modes equal to 0
    nm = remove_m56(nm)

    # Compute the ZPE of the molecule
    zpe = get_ZPE(freqs)
    print(f"The Zero Point Energy for this molecule is {zpe:.4E} J or {zpe * cts.Avogadro / (1000 * cts.calorie):.4F} kcal/mol.")
    print(f"According to the factor of {nm_factor} used, the new ZPE will be {zpe * nm_factor:.4E} J. or {zpe * nm_factor * cts.Avogadro / (1000 * cts.calorie):.4F} kcal/mol.")
    print(f"The velocity for all normal modes at the ZPE is: {math.sqrt(2*zpe/mass)}")

    # Save the XYZ coordinates
    # https://open-babel.readthedocs.io/en/latest/FileFormats/XYZ_cartesian_coordinates_format.html
    lines = []
    lines.append(f"{na}\n")
    lines.append("Molecule in XYZ format generated with MS\n")
    lines += [" {}\t{:10}\t{:10}\t{:10}\n".format(*a) for a in coords]

    with open(f"{simulation_name}.xyz", "w") as xyz:
        xyz.writelines(lines)

    # Convert the XYZ coordinates into a PDB file using Open Babel
    # https://open-babel.readthedocs.io/en/latest/Command-line_tools/babel.html
    mol_conv = subprocess.run(['obabel', '-i', 'xyz', f"{simulation_name}.xyz", '-o', 'pdb', '-O', f"{simulation_name}.pdb"])

    if mol_conv.returncode != 0:
        raise Exception("The generated XYZ file could not be converted to PDB.\nPlease check that you have Open Babel installed and check the XYZ file.")

    # Prepare the PDB for VMD to handle the molecule as a QM region
    # Make the residue "known"
    replace_residue(f"{simulation_name}.pdb", "UNL", "QM")

    # Create a topology file with "something"
    make_rtf(coords)

    # Create the new PDB with the initial coordinates for the MD
    make_pdb(f"{simulation_name}_proc.pdb", f"{simulation_name}.pdb", coords)

    # Ask VMD to create a PSF file based on the latter
    get_psf(f"{simulation_name}")
    make_psf(f"{simulation_name}_proc.psf", f"{simulation_name}.pdb")

    # Get the cell vectors to make a cell in NAMD
    vects = get_cell(f"{simulation_name}")

    # Create the NAMD configuration file to run the simulation
    # Use EnGrad: https://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2016-2017/1376.html
    # Where is the temp file? /dev/shm - https://www.cyberciti.biz/tips/what-is-devshm-and-its-practical-usage.html
    with open('template.namd', 'r') as namd_conf_template:
        template = namd_conf_template.read()

    # Generate normalized arrays of random numbers for the trajectories, and
    # multiply them by the ZPE factor
    # http://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2005-2006/2463.html
    rnums = [nm_factor * 100 * normalize(np.random.random(len(nm[0]))) for t in range(trajectories)]

    with Pool(8) as p:
        # Put all arguments in the same item for each set of random numbers
        items = [(rn, na, nm) for rn in rnums]
        # Compute the force vectors
        force_vectors = p.starmap(compute_vectors, items)

    # Get the current location
    here = os.getcwd()

    # Create all the input files for all trajectories in separate folders
    for traj in range(trajectories):

        # Establish new trajectory's path
        new_dir = os.path.join(here, f"Trajectory_{traj:04d}")

        # If the directory exists, delete it
        if os.path.exists(new_dir):
            shutil.rmtree(new_dir)

        # Create the directory
        os.mkdir(new_dir)

        # Copy the necessary files
        shutil.copy(f"{simulation_name}.pdb", os.path.join(new_dir, f"{simulation_name}.pdb"))
        shutil.copy(f"{simulation_name}_proc.psf", os.path.join(new_dir, f"{simulation_name}_proc.psf"))
        shutil.copy(f"{simulation_name}_proc.pdb", os.path.join(new_dir, f"{simulation_name}_proc.pdb"))

        # Go into the new folder
        os.chdir(new_dir)

        # Temporary RAM folder
        tmppath = os.path.join("/dev/shm", f"{traj:04d}")

        # Fill in NAMD template
        namd_file_content = template.format(name=f"{simulation_name}", cellBV1=vects[0], cellBV2=vects[1], cellBV3=vects[2], cellOrigin=vects[3], tmpfolder=tmppath)

        with open(f"{simulation_name}.namd", "w") as namd_conf:
            namd_conf.write(namd_file_content)

        # Create the new velocities file
        make_pdb(f"{simulation_name}_proc.vel", f"{simulation_name}.pdb", force_vectors[traj])

        # Get out of the folder
        os.chdir(here)


    with Pool(2) as p:

        trajs = [os.path.join(os.path.join(here, f"Trajectory_{t:04d}"), simulation_name) for t in range(trajectories)]

        jobs = p.map(run_NAMD, trajs)