# Job: Single point energy of methiocarb
#
gpus 1
# basis set
basis 6-31++g**
# coordinates file
coordinates methio.xyz
# molecule charge
charge 0
# SCF method (rhf/blyp/b3lyp/etc...): DFT-BLYP
method b3lyp
# add dispersion correction (DFT-D)
dftd yes
# type of the job (energy/gradient/md/minimize/ts): energy
run minimize
end

