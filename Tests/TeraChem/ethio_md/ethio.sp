# Job: Simple trajectory for ethiofencarb
#
# only use 1 gpu
gpus 1
# basis set
basis 6-31g*
# coordinates file
coordinates ethio.xyz
# molecule charge
charge 0
# SCF method (rhf/blyp/b3lyp/etc...): DFT-BLYP
method b3lyp
# add dispersion correction (DFT-D)
dftd yes
# type of the job (energy/gradient/md/minimize/ts): energy
run md
# velocities file
velocities vels.xyz
# md steps
nstep 500
# time in (fs) of each propagation step
timestep 0.5
# rescaling?
thermostat rescale
# rescaling frequency
rescalefreq 100000
# dump orbitals every MD step
orbitalswrtfrq 10
end

