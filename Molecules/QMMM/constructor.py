import os
import sys
import subprocess

nombre = sys.argv[1]

def get_vecs(data):
	content = data.split("\n")
	d = []
	for l in content:
		if "cell" in l:
			d.append(l)
	return d

def get_psf(nombre):
	# First way to create a PSF file
	instructs = """package require topotools
mol new {nam}.pdb
mol reanalyze top
mol bondsrecalc top
set sel [atomselect top "all"]
$sel set segname U
topo guessbonds
topo guessangles
topo guessdihedrals
topo retypebonds
animate write psf {nam}.psf
quit""".format(nam=nombre)
	# Second (more pro) way to create a PSF file
	instructs = """package require psfgen
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_cgenff.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all22_prot.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_carb.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_lipid.rtf
topology /home/zronyj/Git/tccm_tfm/Molecules/toppar/top_all36_prot.rtf
mol new {nam}.pdb
set sel [atomselect top "all"]
$sel set segname U
pdbalias residue HIS HSE   
pdbalias residue HID HSD   
pdbalias residue HIP HSP
set residuos [$sel get resname]
set residuo [lindex $residuos 0]
foreach unidad [$sel get name] {{
	set lastchar [string index $unidad [expr [string length $unidad] - 1]]
	set eval [string is integer $lastchar]
	if {{ $eval == 1 }} then {{
		set newname [string range $unidad 0 [expr [string length $unidad] - 2]]
		pdbalias atom $residuo $unidad $newname
	}}
}}
segment U {{ pdb {nam}.pdb }}
coordpdb {nam}.pdb U
guesscoord
writepdb {nam}_proc.pdb
writepsf {nam}_proc.psf
quit
""".format(nam=nombre)
	with open(f"{nombre}_psf_gen", "w") as psf_script:
		psf_script.write(instructs)
	Xproc = subprocess.run(['vmd', '-dispdev', 'text', '-e', f"{nombre}_psf_gen"])
	os.remove(f"{nombre}_psf_gen")

def get_cell(nombre):
	xtr = """mol new {nam}_proc.psf
mol addfile {nam}_proc.pdb
set all [atomselect $molid all] 
set minmax [measure minmax $all] 
set vec [vecsub [lindex $minmax 1] [lindex $minmax 0]] 
puts "cellBasisVector1 [lindex $vec 0] 0 0" 
puts "cellBasisVector2 0 [lindex $vec 1] 0" 
puts "cellBasisVector3 0 0 [lindex $vec 2]" 
set center [measure center $all] 
puts "cellOrigin $center" 
$all delete
mol delete top
quit""".format(nam=nombre)
	with open(f"{nombre}_vmd_extract", "w") as extractor:
		extractor.write(xtr)
	vec_data = subprocess.check_output(["vmd","-dispdev","text","-e", f"{nombre}_vmd_extract"])
	os.remove(nombre + "_vmd_extract")
	vecs = get_vecs(vec_data.decode('utf8'))
	return vecs

get_psf(nombre)
v = get_cell(nombre)

with open('test.namd', 'r') as f:
	template = f.read()

final = template.format(name=nombre, cellBV1=v[0], cellBV2=v[1], cellBV3=v[2], cellOrigin=v[3])

with open(f"{nombre}.namd", "w") as g:
	g.write(final)