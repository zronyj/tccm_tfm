#!/bin/bash

# MOPAC launcher script
module load mopac/2018

#SBATCH --ntasks=2
#SBATCH --account=herbicidas
#SBATCH --partition=emtccm
#SBATCH --job-name=optmol
#SBATCH --time=3:00:00


#run the job
MOPAC2016.exe histidine_rm1.mop
MOPAC2016.exe glutamic_acid_rm1.mop
