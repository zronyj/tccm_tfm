

                     SUMMARY OF  RM1 CALCULATION

                                                       MOPAC v22.0.4 Linux
                                                       Wed Nov  2 17:39:11 2022

           Empirical Formula: C6 H9 N3 O2  =    20 atoms

 CHARGE=0 RM1 PRECISE XYZ
 Histidine: Geometry optimized using OpenBabel with MMFF94s



     GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING (EF).     
     SCF FIELD WAS ACHIEVED                                   

          HEAT OF FORMATION       =        -67.49443 KCAL/MOL =    -282.39669 KJ/MOL
          GRADIENT NORM           =          0.04574          =       0.01023 PER ATOM
          DIPOLE                  =          1.38150 DEBYE   POINT GROUP:  C1  
          NO. OF FILLED LEVELS    =         30
          IONIZATION POTENTIAL    =          9.448032 EV
          HOMO LUMO ENERGIES (EV) =         -9.448  0.555
          MOLECULAR WEIGHT        =        155.1560
          COSMO AREA              =        176.71 SQUARE ANGSTROMS
          COSMO VOLUME            =        184.17 CUBIC ANGSTROMS

          MOLECULAR DIMENSIONS (Angstroms)

            Atom       Atom       Distance
            H    19    H    13     7.37569
            H    12    O    11     5.21087
            H    16    H    20     3.81576
          SCF CALCULATIONS        =        100
          WALL-CLOCK TIME         =      0.375 SECONDS
          COMPUTATION TIME        =      2.753 SECONDS


          FINAL GEOMETRY OBTAINED
 CHARGE=0 RM1 PRECISE XYZ
 Histidine: Geometry optimized using OpenBabel with MMFF94s

  C     0.23687576 +1  -0.32302032 +1   1.30459172 +1
  N    -0.86181182 +1   0.47261750 +1   0.97774704 +1
  C    -0.93875806 +1   0.45594074 +1  -0.37625427 +1
  N     0.07871226 +1  -0.32632106 +1  -0.92268411 +1
  C     0.82652676 +1  -0.82727341 +1   0.14645275 +1
  C     2.01139575 +1  -1.69142883 +1  -0.01663647 +1
  C     1.57367914 +1  -3.17168433 +1  -0.07128861 +1
  C     0.61013775 +1  -3.29817932 +1  -1.25757900 +1
  N     2.73866128 +1  -4.02458986 +1  -0.21451920 +1
  O    -0.72201117 +1  -3.14797977 +1  -1.08992633 +1
  O     0.96761115 +1  -3.53043660 +1  -2.39896353 +1
  H     0.54045464 +1  -0.47946981 +1   2.33531662 +1
  H    -1.68956097 +1   0.97742486 +1  -0.97433143 +1
  H     0.24735153 +1  -0.52075382 +1  -1.90041347 +1
  H     2.71161520 +1  -1.53452245 +1   0.83302983 +1
  H     2.59883720 +1  -1.41115495 +1  -0.91862990 +1
  H     1.05302948 +1  -3.44983390 +1   0.88701883 +1
  H     3.28493813 +1  -3.84251418 +1  -1.04974424 +1
  H     2.53067630 +1  -5.01226828 +1  -0.12935969 +1
  H    -0.92213111 +1  -2.89243560 +1  -0.18169668 +1
 
