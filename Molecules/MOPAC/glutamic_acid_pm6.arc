

                     SUMMARY OF  PM6 CALCULATION

                                                       MOPAC v22.0.4 Linux
                                                       Wed Nov  2 17:38:58 2022

           Empirical Formula: C5 H9 N O4  =    19 atoms

 CHARGE=0 PM6 PRECISE XYZ
 Glutamic Acid: Geometry optimized using OpenBabel with MMFF94s



     GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING (EF).     
     SCF FIELD WAS ACHIEVED                                   

          HEAT OF FORMATION       =       -183.12241 KCAL/MOL =    -766.18418 KJ/MOL
          GRADIENT NORM           =          0.04605          =       0.01056 PER ATOM
          DIPOLE                  =          4.86880 DEBYE   POINT GROUP:  C1  
          NO. OF FILLED LEVELS    =         29
          IONIZATION POTENTIAL    =         10.103993 EV
          HOMO LUMO ENERGIES (EV) =        -10.104 -0.253
          MOLECULAR WEIGHT        =        147.1304
          COSMO AREA              =        166.36 SQUARE ANGSTROMS
          COSMO VOLUME            =        168.00 CUBIC ANGSTROMS

          MOLECULAR DIMENSIONS (Angstroms)

            Atom       Atom       Distance
            H    19    O     6     6.34469
            H    15    H    17     4.30301
            H    16    H    11     3.91772
          SCF CALCULATIONS        =        137
          WALL-CLOCK TIME         =      0.445 SECONDS
          COMPUTATION TIME        =      3.338 SECONDS


          FINAL GEOMETRY OBTAINED
 CHARGE=0 PM6 PRECISE XYZ
 Glutamic Acid: Geometry optimized using OpenBabel with MMFF94s

  C     1.34967591 +1  -0.20397490 +1  -0.03560142 +1
  C     0.58465068 +1   0.72806283 +1  -1.00071027 +1
  C     0.98539217 +1   0.40659510 +1  -2.45415031 +1
  N    -0.86523918 +1   0.53791433 +1  -0.76973938 +1
  O     2.05329062 +1   1.08573581 +1  -2.96594569 +1
  O     0.43186339 +1  -0.39204054 +1  -3.16797200 +1
  C     2.87243399 +1  -0.08260366 +1  -0.13813773 +1
  C     3.32773409 +1   1.33146372 +1   0.10756983 +1
  O     3.93588433 +1   1.60751971 +1   1.30249955 +1
  O     3.20290568 +1   2.25985241 +1  -0.66192314 +1
  H     1.05667124 +1  -1.26020974 +1  -0.22204085 +1
  H     1.00775511 +1   0.01493211 +1   1.00000691 +1
  H     0.83914533 +1   1.80115050 +1  -0.77189587 +1
  H    -1.17550134 +1  -0.37144375 +1  -1.11278351 +1
  H    -1.40732807 +1   1.24601730 +1  -1.25933843 +1
  H     2.47449381 +1   1.76637807 +1  -2.34878484 +1
  H     3.21532768 +1  -0.39688609 +1  -1.15557716 +1
  H     3.35322421 +1  -0.80575771 +1   0.55080106 +1
  H     3.99594491 +1   0.84978268 +1   1.93205013 +1
 
