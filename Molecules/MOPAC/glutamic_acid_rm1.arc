

                     SUMMARY OF  RM1 CALCULATION

                                                       MOPAC v22.0.4 Linux
                                                       Wed Nov  2 17:38:51 2022

           Empirical Formula: C5 H9 N O4  =    19 atoms

 CHARGE=0 RM1 PRECISE XYZ
 Glutamic Acid: Geometry optimized using OpenBabel with MMFF94s



     GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING (EF).     
     SCF FIELD WAS ACHIEVED                                   

          HEAT OF FORMATION       =       -186.51091 KCAL/MOL =    -780.36167 KJ/MOL
          GRADIENT NORM           =          0.04468          =       0.01025 PER ATOM
          DIPOLE                  =          3.66574 DEBYE   POINT GROUP:  C1  
          NO. OF FILLED LEVELS    =         29
          IONIZATION POTENTIAL    =         10.073262 EV
          HOMO LUMO ENERGIES (EV) =        -10.073  0.622
          MOLECULAR WEIGHT        =        147.1304
          COSMO AREA              =        167.13 SQUARE ANGSTROMS
          COSMO VOLUME            =        170.35 CUBIC ANGSTROMS

          MOLECULAR DIMENSIONS (Angstroms)

            Atom       Atom       Distance
            H    19    O     6     5.98177
            H    15    H    18     4.53896
            H    16    H    14     4.01611
          SCF CALCULATIONS        =        230
          WALL-CLOCK TIME         =      0.781 SECONDS
          COMPUTATION TIME        =      5.982 SECONDS


          FINAL GEOMETRY OBTAINED
 CHARGE=0 RM1 PRECISE XYZ
 Glutamic Acid: Geometry optimized using OpenBabel with MMFF94s

  C     1.46723996 +1  -0.24815621 +1  -0.11880391 +1
  C     0.65051986 +1   0.74949483 +1  -0.96402169 +1
  C     0.93404607 +1   0.43153387 +1  -2.43825970 +1
  N    -0.76079391 +1   0.59793490 +1  -0.64600821 +1
  O     1.78775631 +1   1.17753924 +1  -3.16952805 +1
  O     0.40760920 +1  -0.49398330 +1  -3.03020704 +1
  C     2.96588794 +1  -0.06503298 +1  -0.32659675 +1
  C     3.37565297 +1   1.30650297 +1   0.14381405 +1
  O     3.61490440 +1   1.52754512 +1   1.45518076 +1
  O     3.49924412 +1   2.27946521 +1  -0.57987976 +1
  H     1.18157616 +1  -1.29386618 +1  -0.36788069 +1
  H     1.20304228 +1  -0.13494106 +1   0.95519502 +1
  H     0.94219785 +1   1.80772165 +1  -0.71671584 +1
  H    -1.13287249 +1  -0.32367548 +1  -0.85138007 +1
  H    -1.34525529 +1   1.31988975 +1  -1.05030242 +1
  H     2.18034035 +1   1.88001142 +1  -2.63646267 +1
  H     3.25536162 +1  -0.20312906 +1  -1.39187098 +1
  H     3.53652579 +1  -0.86016770 +1   0.20060979 +1
  H     3.45821173 +1   0.72721669 +1   1.96820192 +1
 
