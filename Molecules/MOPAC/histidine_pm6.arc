

                     SUMMARY OF  PM6 CALCULATION

                                                       MOPAC v22.0.4 Linux
                                                       Wed Nov  2 17:39:15 2022

           Empirical Formula: C6 H9 N3 O2  =    20 atoms

 CHARGE=0 PM6 PRECISE XYZ
 Geometry optimized using OpenBabel with MMFF94s



     GEOMETRY OPTIMISED USING EIGENVECTOR FOLLOWING (EF).     
     SCF FIELD WAS ACHIEVED                                   

          HEAT OF FORMATION       =        -57.65340 KCAL/MOL =    -241.22181 KJ/MOL
          GRADIENT NORM           =          0.02411          =       0.00539 PER ATOM
          DIPOLE                  =          2.11289 DEBYE   POINT GROUP:  C1  
          NO. OF FILLED LEVELS    =         30
          IONIZATION POTENTIAL    =          9.829186 EV
          HOMO LUMO ENERGIES (EV) =         -9.829 -0.027
          MOLECULAR WEIGHT        =        155.1560
          COSMO AREA              =        178.52 SQUARE ANGSTROMS
          COSMO VOLUME            =        185.54 CUBIC ANGSTROMS

          MOLECULAR DIMENSIONS (Angstroms)

            Atom       Atom       Distance
            H    19    H    13     7.58559
            H    12    O    11     5.01444
            H    16    H    20     3.60373
          SCF CALCULATIONS        =        114
          WALL-CLOCK TIME         =      0.422 SECONDS
          COMPUTATION TIME        =      3.178 SECONDS


          FINAL GEOMETRY OBTAINED
 CHARGE=0 PM6 PRECISE XYZ
 Geometry optimized using OpenBabel with MMFF94s

  C     0.11090015 +1  -0.50963545 +1   1.28867309 +1
  N    -0.93905091 +1   0.37493245 +1   1.04677118 +1
  C    -0.90372091 +1   0.66486773 +1  -0.27307370 +1
  N     0.15543909 +1  -0.02013392 +1  -0.89883656 +1
  C     0.80139645 +1  -0.77388957 +1   0.09888575 +1
  C     1.96717946 +1  -1.64752497 +1  -0.16061535 +1
  C     1.58695886 +1  -3.14507648 +1  -0.10747433 +1
  C     0.63319695 +1  -3.47317728 +1  -1.27257337 +1
  N     2.82482120 +1  -3.95231333 +1  -0.15939808 +1
  O    -0.68758013 +1  -3.13715412 +1  -1.11320077 +1
  O     0.94289845 +1  -4.01168933 +1  -2.30451242 +1
  H     0.31145044 +1  -0.88137125 +1   2.27883427 +1
  H    -1.57907897 +1   1.32589106 +1  -0.79646152 +1
  H     0.40017664 +1  -0.00343172 +1  -1.87268890 +1
  H     2.75542677 +1  -1.46212898 +1   0.61208882 +1
  H     2.46295607 +1  -1.40640376 +1  -1.12454537 +1
  H     1.06968353 +1  -3.36230417 +1   0.86742461 +1
  H     3.28382258 +1  -3.86948299 +1  -1.06574917 +1
  H     2.62499278 +1  -4.93928177 +1  -0.01253862 +1
  H    -0.90434966 +1  -2.64004540 +1  -0.28377931 +1
 
