# TCCM_TFM



## Setting up

1. Downloading and installing
    * MOPAC --> MOPAC2016
    * Orca --> Orca 5.0.3
    * NAMD2 --> NAMD 2.14 CUDA
    * NAMD2mc --> NAMD 2.14 multicore
    * Venus
    * NWChem ?

## Running calculations

1. Drawing **histidine** and **glutamic acid** in Avogadro, and saving them into XYZ format.
2. Attempting to optimize everything in MOPAC and get coordinates.
    * MOPAC2016 installed and running
    * MOPAC used to optimize geometry at RM1 and PM6 levels
3. Attempting to optimize geometry in Orca and get coordinates.
    * Orca 5.0.3 installed and running (both in Scarlett and Axis)
