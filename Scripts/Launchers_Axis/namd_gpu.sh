#!/bin/bash
#              A line beginning with #PBS is a PBS directive.
#              PBS directives must come first; any directives
#              after the first executable statement are ignored.
#PBS -N namd_rony
#PBS -m ae
#          Specify the number of nodes requested and the
#          number of processors per node. 
#PBS -l nodes=1:ppn=8
#PBS -W x=GRES:gpu@1
#
###PBS -o stdout_file
###PBS -e stderr_file
#          Specify the maximum cpu and wall clock time. The wall
#          clock time should take possible queue waiting time into
#          account.  Format:   hhhh:mm:ss   hours:minutes:seconds
#PBS -l    cput=800:00:00
#PBS -l walltime=800:00:00
#          Specify the maximum amount of physical memory required per process.
#          kb for kilobytes, mb for megabytes, gb for gigabytes.
###PBS -l pmem=2048mb
##########################################
#                                        #
#   Output some useful job information.  #
#                                        #
##########################################
NCPU=`wc -l < $PBS_NODEFILE`
# Please set INPUT env variables
INPUT1=$HOME/NAMD_Tests/config.in
echo ------------------------------------------------------
echo ' This job is allocated on '${NCPU}' cpu(s)'
echo ' Job is running on node(s): '
echo ------------------------------------------------------
cat $PBS_NODEFILE
WDIR=/scratch/$USER.$PBS_JOBID
cp $INPUT1 $WDIR
cd $WDIR
pwd
date
echo ${PBS_NUM_PPN}
/home/leo/Prove_pbs/NAMD/PBS_tes/charmrun /home/leo/Prove_pbs/NAMD/PBS_tes/namd2 +p$PBS_NUM_PPN config.in > config.log
ls
TOSAVE=$HOME/NAMD_Tests
if [-d $TOSAVE ];
then
        echo "$TOSAVE directory exists!"
else
        mkdir $TOSAVE
        echo "$TOSAVE directory created!"
fi
mv $WDIR/* $TOSAVE/
date

