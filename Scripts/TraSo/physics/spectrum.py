import os
import numpy as np
from matplotlib import pyplot as plt

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

def distance_r3(atom1, atom2):
	d = [(atom1[i] - atom2[i])**2 for i in range(1,4)]
	return sum(d)**0.5

def get_fragment_mass(atoms, periodic=periodic):
	tot_mass = 0
	for a in atoms:
		tot_mass += periodic[a[0]]
	return tot_mass

def adjacency_matrix(file_name, threshold=1.7):
	with open(file_name, "r") as f:
		data = f.readlines()
	num_atoms = len(data) - 2
	atoms = []
	lt_mat = []
	for l in data[1:-1]:
		temp = l.split()
		atoms.append([temp[2], float(temp[6]), float(temp[7]), float(temp[8])])
		lt_mat.append([])
		for a in atoms:
			d = distance_r3(a, atoms[-1])
			if d <= threshold:
				lt_mat[-1].append(1)
			else:
				lt_mat[-1].append(0)
	adj_mat = np.zeros([num_atoms, num_atoms])
	for i in range(len(lt_mat)):
		for j in range(len(lt_mat[i])):
			if i != j:
				adj_mat[i][j] = lt_mat[i][j]
				adj_mat[j][i] = lt_mat[i][j]
	return atoms, adj_mat

def get_fragments(atoms, adjm, pte=periodic):
	num_atoms = len(atoms)
	groups = [0] * num_atoms
	frag = 0
	for i in range(num_atoms):
		if groups[i] == 0:
			frag += 1
			groups[i] = frag
		for j in range(i + 1, num_atoms):
			if adjm[i][j] == 1:
				groups[j] = groups[i]
	fragments = [[] for f in range(frag)]
	for i, g in enumerate(groups):
		fragments[g - 1].append(atoms[i])
	masses = [get_fragment_mass(f, pte) for f in fragments]
	return masses

def compute_spectrum(sim_name, wd, thr=1.7):
	all_masses = []
	trajectories = os.listdir(wd)
	trajectories = [t for t in trajectories if "Trajectory_" in t]
	good_trajs = 0
	for traj in trajectories:
		file_name = os.path.join(wd, traj, f"{sim_name}_out.restart.coor")
		try:
			atms, adjm = adjacency_matrix(file_name, thr)
			all_masses += get_fragments(atms, adjm)
			good_trajs += 1
		except Exception as e:
			print(f"Trajectory {traj} did not produce any final coordinates file!")
	unique_masses = set(all_masses)
	spectrum = []
	for f in unique_masses:
		spectrum.append([f, all_masses.count(f)])
	spectrum.sort(key=lambda x: x[0])
	return spectrum, good_trajs

def make_spectrum(sim_name, wd, molecule, energy, thr=1.7, label_thrs=0.25):
	spctr, g_trajs = compute_spectrum(sim_name, wd, thr)
	X, Y = zip(*spctr)
	rel = max(Y)
	Y = [y/rel * 100 for y in Y]
	Y0 = [0]*len(Y)
	plt.figure(figsize=(16, 9))
	plt.vlines(X, Y0, Y, color="red", linestyles="solid")
	for i, v in enumerate(Y):
		if v >= (label_thrs * 100):
			plt.annotate(f"{X[i]:.2f}", (X[i] - 0.5, v + 1.5), fontsize=6, rotation=90)
	plt.title((f"Theoretical Mass Spectrum for {molecule}\n"
		rf"{g_trajs} trajectories at {energy} $kcal / mol$ with bond threshold of {thr} $\AA$"))
	plt.xlabel("Fragment mass (g/mol)")
	plt.ylabel("Relative abundance (%)")
	plt.ylim([0,115])
	plt.xlim([-5, max(X) + 5])
	plt.savefig(f"{sim_name}_ms.png", dpi=300)
	plt.cla()