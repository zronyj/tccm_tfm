""" Module to make IO files to run in Orca

orca_io contains functions to create inp files or parse data from
output files.
"""

import os                  # A library to manage the file system
from subprocess import run # Method to run external commands
from math import ceil      # Method to round up a float
import numpy as np         # Library to do basic scientific computing

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

def make_run_input(xyz_file, orca_path, method="B3LYP", basis="6-31G**",
    opt="", freq="Freq", cores=8, memory=3072, charge=0, mult=1):
    r"""Make an Orca input file, and perform the calculation

    This function is intended to create the input file for a
    frequencies (thermochemistry) calculation in Orca.

    Parameters
    ----------
    xyz_file : str
        The full path of the XYZ file, with the extension
    orca_path : str
        The complete path to the orca executable[3] for the
        MPI version to work
    method : str
        Either "HF", or the DFT functional[2] (e.g. PBE,
        B3LYP, M06)
    basis : str
        Any basis set available in Orca[1] (e.g. 6-31G,
        def2-TZVP, aug-cc-pVDZ)
    opt : str
        Can be "Opt" or nothing at all. Orca will optimize
        the molecular geometry in the former.
    freq : str
        Can be "Freq" for small molecules or "NumFreq"
        for larger molecules
    cores : int
        The number of processing cores to use in the given
        computer/node.
    memory : int
        The amount of memory to be used **per core** in the
        given computer/node.
    charge : int
        Total charge of the molecule
    mult : int
        Spin multiplicity of the molecule

    Returns
    -------
    None
        Two files: name.inp and name.out with the input
        and output data of the Orca calculation.

    References
    ----------
    ..  [1] Basis Sets. ORCA Input Library,
        https://sites.google.com/site/orcainputlibrary/basis-sets
    ..  [2] DFT calculations. ORCA Input Library,
        https://sites.google.com/site/orcainputlibrary/dft-calculations
    ..  [3] Setting up ORCA. ORCA Input Library,
        https://sites.google.com/site/orcainputlibrary/setting-up-orca
    """
    xyz_file = xyz_file[:-4]
    # Orca input template
    template = f"""! {method} {basis} {opt} {freq}
%maxcore {memory}
%pal
nprocs {cores}
end
*xyz {charge} {mult}
"""
    # Read the XYZ file and extract the data
    with open(f"{xyz_file}.xyz", "r") as xyz:
        data = xyz.readlines()

    # Add the atoms and coordinates
    for i, line in enumerate(data):
        if i > 1:
            template += line
    # Finish the file
    template += "*\n"

    # Save the file
    with open(f"{xyz_file}.inp", "w") as o_inp:
        o_inp.write(template)

    # Run the calculation
    with open(f"{xyz_file}.out", "w") as out:
        orca_run = run([orca_path, f"{xyz_file}.inp"],
            stdout=out)

    # Inform if the process finished correctly
    print(f"Orca finished with code {orca_run.returncode}")

    # Clean the place
    for extension in [".densities", ".engrad", ".gbw",
        "_property.txt", "_trj.xyz"]:
        os.remove(f"{xyz_file}{extension}")


def parse_output(nombre, periodic=periodic):
    r"""Extract data from an Orca output file

    This function is intended to extract the number of atoms,
    total mass, atom mass, coordinates, normal modes, and
    frequencies of the Orca output.

    Parameters
    ----------
    nombre : str
        The name of the output file, with the extension.

    Returns
    -------
    na : int
        The number of atoms in the molecule
    mass : float
        The total mass of the molecule.
    masses : list of float
        A list of the masses of all atoms in the molecule
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    nm : list of list of float
        A list with lists of numbers corresponding to the normal
        modes in the form of XYZXYZ... vectors.
    freqs : list of float
        A list of the frequencies (eigenvalues) of the normal
        modes
    """
    with open(nombre, 'r') as f:
        data = f.readlines()

    na_init = 0             # Line number for number of atoms
    coord_init = 0          # Line number for atom coordinates
    nm_init = 0             # Line number for normal modes
    freqs_init = 0          # Line number for frequencies

    # Check where are the lines of interest
    for ln in range(len(data)):
        # Find the line with the number of atoms in the molecule
        if "Number of atoms" in data[ln] and not na_init:
            na_init = ln
        # Find the line where the cartesian coordinates of the molecule start
        if "> *xyz" in data[ln]:
            coord_init = ln + 1
        # Find the line where the frequencies of the molecule start
        if "VIBRATIONAL FREQUENCIES" in data[ln]:
            freqs_init = ln + 5
        # Find the line where the normal modes of the molecule start
        if "NORMAL MODES" in data[ln] and not nm_init:
            nm_init = ln + 7

    # Extract the number of atoms in the molecule
    na_line = data[na_init].split()
    na = int(na_line[-1])

    # Get the coordinates of all atoms
    coord_lines = data[coord_init: coord_init + na]
    coord_lines = [l.split() for l in coord_lines]
    coords = []
    for l in coord_lines:
        coords.append(
            [float(l[i]) if i > 2 else l[i] for i in range(2, len(l))]
            )

    # Get the masses for each atom individually
    masses = np.array([ periodic[c[0]] for c in coords ])
    mass = masses.sum()

    # Get the frequencies
    freqs_num = na * 3
    freqs_lines = data[freqs_init: freqs_init + freqs_num]
    freqs_lines = [l.split() for l in freqs_lines]
    freqs = np.array([float(l[1]) for l in freqs_lines if float(l[1]) != 0])
    if all(freqs) < 0:
        raise ValueError("WARNING! Negative frequency detected!")

    # Get the normal modes
    # These come in groups of 6, as column-vectors of XYZXYZXYZ... components
    # https://gaussian.com/vib/
    nm_num = na * 3           # 3 normal modes per coordinate
    nm_chunks = ceil(na / 2)  # It's actually 3 * na / 6 for the 6 columns
    nm_lines = []

    # For each 6-column chunk of data ...
    for c in range(nm_chunks):
        temp_from = nm_init + c * (nm_num + 1)     # Compute the starting coord
        temp_to = nm_init + (c + 1) * (nm_num + 1) # COmpute the end coord
        nm_lines += data[temp_from : temp_to]      # Save the data

    # At this point the normal modes are stored as a list of strings

    nm_lines = [l.split() for l in nm_lines]       # Split the strings

    nm = [[] for r in range(nm_num)]               # Prepare a list for the NMs

    # Iterating over all the lists of broken strings
    for i in range(len(nm_lines)):
        ent = i%(nm_num + 1) - 1                   # Compute the entry number
        if ent != -1:                              # If it's not the first ...
            nm[ent] += [float(n) for n in nm_lines[i][1:]]

    # Create the normal modes matrix using NumPy
    nm = np.array(nm)

    # Check that the normal modes don't include translations and rotations
    # (first 5 or 6 normal modes)
    # https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/Vibrational_Spectroscopy/Vibrational_Modes/Introduction_to_Vibrations
    check = nm.sum(axis=0)
    if check[5] <= 1E-15:
        nm = nm[:,6:].T.reshape(3*na - 6, na, 3)
    else:
        nm = nm[:,5:].T.reshape(3*na - 5, na, 3)

    return na, mass, masses, coords, nm, freqs