""" Module to make IO files for a QM/MM run in NAMD

namd_io contains functions to create the PDB, PSF, RTF and VEL files
to run a QM/MM molecular dynamics simulation in NAMD.
"""

import os                  # A library to manage the file system
from shutil import rmtree  # A method to delete folders
from subprocess import run # A method to run external commands

# Masses for some elements
periodic = {"H": 1.008,
    "Li": 6.941,
    "Be": 9.012,
    "B": 10.811,
    "C": 12.011,
    "N": 14.007,
    "O": 15.999,
    "F": 18.998,
    "Na": 22.990,
    "Mg": 24.305,
    "Al": 26.982,
    "Si": 28.086,
    "P": 30.974,
    "S": 32.065,
    "Cl": 35.453}

def get_pdb_data(xyz_file_name):
    r"""Make a PDB file for NAMD, from an XYZ file

    This function is intended to create the initial
    coordinates file.

    Parameters
    ----------
    xyz_file_name : str
        The name of the XYZ file, without the extension.

    Returns
    -------
    pdb_template : str
        A template for a future PDB file for NAMD.
    """
    # Convert the XYZ coordinates into a PDB file using Open Babel
    # https://open-babel.readthedocs.io/en/latest/Command-line_tools/babel.html
    temp_pdb = run(['obabel', '-i', 'xyz', f"{xyz_file_name}.xyz",
        '-o', 'pdb', '-O', f"temp_{xyz_file_name}.pdb"])
    
    # Open the newly created PDB
    with open(f"temp_{xyz_file_name}.pdb", 'r') as old_pdb:
        pdb_data = old_pdb.readlines()

        # Iterate over the file contents
        for i, l in enumerate(pdb_data):
            if (('ATOM' in l) or ('HETATM' in l)):   # If an atom is found ...
                old_line = l.split()                 # ... split into strings

                # And re-build the line without coordinates (as a template)
                new_line = [f"{old_line[0]:<6}", f"{old_line[1]:>4}",
                f" {old_line[2]:<3}", " QM", "A", "  1    ",  "{:>7.3f}",
                "{:>7.3f}", "{:>7.3f}", " 1.00", " 0.00",
                f"{old_line[-1]:>11}  "]

                # Make a single string out of the new data
                pdb_data[i] = " ".join(new_line)

            # Add a "new line" character if needed
            if pdb_data[i][-1] != "\n":
                pdb_data[i] = pdb_data[i] + "\n"

        # Re-pack all data into a single string
        pdb_template = "".join(pdb_data)

    # Delete the PDB created by OpenBabel
    os.remove(f"temp_{xyz_file_name}.pdb")
    return pdb_template


def make_rtf(atoms):
    r"""Make an RTF parameter file for NAMD from the atoms list

    Parameters
    ----------
    atoms : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates

    Returns
    -------
    None
        It creates a file: QM.rtf with no other data than the atomic
        identities and some formatting.
    """
    lines = []
    # Create the first line of the RTF file and add it to the list
    lines.append("\n\nread rtf card append\n\nRESI QM         0.00\n\n")

    # Add the information on every atom symbol
    for atom in atoms:
        lines.append(f"ATOM {atom[0]}    {atom[0]}      0.00\n")

    # Add the end of the file as a word
    lines.append("end\n\n")

    # Save the file
    with open("QM.rtf", "w") as f:
        f.writelines(lines)

def make_psf(na, coords, nname, periodic=periodic):
    r"""Make an PSF topology file for NAMD

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates
    nname : str
        The name of the PSF file to be created
    periodic : dict
        The periodic table; the keys are the symbols (str) and the
        values (float) are the atomic masses

    Returns
    -------
    None
        It creates a file the atomic identities, the masses and
        some formatting.
    """
    # First line
    lines = [
        ("PSF\n\n       1 !NTITLE\n REMARKS TraSo-generated "
         "NAMD/X-Plor PSF structure file\n\n")
            ]

    # Atoms section
    lines.append(f"{na:8} !NATOM\n")

    # Add each atom symbol and mass
    for i, a in enumerate(coords):
        lines.append(
            (f"{i + 1:8} U    1    QM  {a[0]}    {a[0]}      "
             f"0.000000  {periodic[a[0]]:14.4F}           0\n")
            )

    # Every other section
    lines.append("\n       0 !NBOND: bonds\n\n\n")
    lines.append("       0 !NTHETA: angles\n\n\n")
    lines.append("       0 !NPHI: dihedrals\n\n\n")
    lines.append("       0 !NIMPHI: impropers\n\n\n")
    lines.append("       0 !NDON: donors\n\n\n")
    lines.append("       0 !NACC: acceptors\n\n\n")
    lines.append("       0 !NNB\n\n\n")
    lines.append("       1       0 !NGRP\n\n")

    with open(nname, "w") as psf:
        psf.writelines(lines)

def make_pdb(na, nname, base, coords):
    r"""Make a PDB file for NAMD with initial coordinates/velocities

    This function is different from the other, because NAMD does not
    use a regular PDB as initial condition, but a simpler PDB.

    Parameters
    ----------
    na : int
        The number of atoms in the molecule
    nname : str
        The name of the PDB file to be created
    base : str
        The name of the PDB file to be read
    coords : list of [str, float, float, float]
        A list of the atom symbol and its X, Y and Z coordinates or
        velocities

    Raises
    ------
    IndexError
        If the number of atoms in the file is not the same as the
        number of atoms entered as parameter

    Returns
    -------
    None
        It creates a file the atomic identities and the coordinates/
        velocities.
    """
    # Read the original PDB file and get the data
    with open(base, "r") as f:
        data = f.readlines()

    # Check whether the information in the PDB lists the atoms as ATOM
    # If so, get the starting and ending lines
    init, final = 0, 0
    for l in range(len(data)):
        if (("ATOM" in data[l]) and
                not("ATOM" in data[l-1])
                and (init == 0)):
            init = l
        if (("ATOM" in data[l]) and
                not("ATOM" in data[l+1])
                and (final == 0)):
            final = l

    # If no atoms were detected, then the atoms are listed as HETATM
    # If so, do the same as before
    if final - init == 0:
        for l in range(len(data)):
            if (("HETATM" in data[l]) and
                    not("HETATM" in data[l-1])
                    and (init == 0)):
                init = l
            if (("HETATM" in data[l]) and
                    not("HETATM" in data[l+1])
                    and (final == 0)):
                final = l

    # Sanity Check: Compute the number of atoms in the file and check 
    # if it matches the number of atoms entered as parameter.
    # If not, raise an error.
    num_atoms = len(data[init: final+1])
    if num_atoms != na:
        print(data[init: final+1])
        raise IndexError(
            (f"The initial file and the given coordinates have a different "
             f"amount of atoms!\n{num_atoms} and {len(coords)} respectively.")
            )

    lines = []
    # If the file is supposed to contain coordinates ...
    if nname[-3:] == "pdb":
        lines.append(
            ("CRYST1    0.000    0.000    0.000  "
             "90.00  90.00  90.00 P 1           1\n")
                )

    # If the file is supposed to contain velocities ...
    else:
        lines.append("REMARK  RESTART VELOCITIES WRITTEN BY TRASO FOR NAMD\n")

    # Add the coordinates/velocities to the file
    for l in range(num_atoms):
        temp = data[init + l].split()
        lines.append(
            (f"ATOM{int(temp[1]):7}  {temp[-1]}    QM X   1"
             f"{coords[l][-3]:12.3F}{coords[l][-2]:8.3F}{coords[l][-1]:8.3F}"
             f"  1.00  0.00      U    {temp[-1]}\n")
                )

    # Add the final line of the file
    lines.append("END\n")

    # Save the file
    with open(nname, "w") as g:
        g.writelines(lines)

def run_NAMD(path_nam, namd_path, cores=2):
    r"""Run a NAMD simulation

    This function is intended to run the input file for an
    MD simulation and running it accordingly.

    Parameters
    ----------
    path_nam : str
        The complete path of the NAMD input file, without the extension
    namd_path : str
        The complete path to the NAMD executable
    cores : int
        The number of processing cores to use in the given
        computer/node.

    Returns
    -------
    None
        Several files:
        ..  name.log - the NAMD output
        ..  name.dcd - the NAMD trajectory
        ..  name.coor - the final coordinates of the atoms
        ..  name.restart.coor - the final coordinates of the atoms
        ..  name.restart.vel - the final velocities of the atoms
        ..  name.restart.xsc - the extended system configuration
    """
    nam = os.path.basename(path_nam)                # Get the name of the run
    folder = os.path.dirname(path_nam)              # Get the name/number of the trajectory 
    cwd = os.path.dirname(folder)                   # Get the name of the WD
    ramdir = os.path.join("/dev/shm", folder[-4:])  # Get the name of the RAM directory
    # Show me what trajectory are you working in
    print("-"*21 + "> " + os.path.basename(folder) + " <" + "-"*20)
    if os.path.exists(ramdir):                      # If the RAM directory exists, delete it!
        rmtree(ramdir)
    os.mkdir(ramdir)                                # Create the RAM directory
    os.chdir(folder)
    with open(f"{nam}.log", "w") as f:              # Open a log file
        # Run NAMD!
        log = run([namd_path, f"+p{cores}", f"{nam}.namd"], stdout=f)
    os.chdir(cwd)                                   # Get out of the trajectory's folder
#    rmtree(ramdir)                                  # Delete the RAM directory
