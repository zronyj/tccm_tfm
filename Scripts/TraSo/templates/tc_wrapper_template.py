#!/usr/bin/env python3

import os
import sys
import subprocess

# TeraChem input first line (configuration)
tc_conf_line = """# Job: Single point energy and gradient
#
# GPUs to be used
gpus 1
# Basis set
basis {bset}
# SCF method (rhf/blyp/b3lyp/etc...)
method {theo}
# Include dispersion correction for long range vdW forces
dispersion d3
# Maximum number of iterations for the Self Consistent Field
maxit {scf}
# Coordinates file
coordinates qmmm.xyz
# Total charge of the molecule
charge 0
# Multiplicity of the electrons in the molecule
spinmult 1
# Compute the charges of the molecule
poptype mulliken
# type of the job (energy/gradient/md/minimize/ts)
run gradient
end"""

# =============================================================================
# File names and paths
# =============================================================================

# Get the name of NAMD's output, as this script's input
input_name = sys.argv[1]

# Get the file's path
base_directory = os.path.dirname(input_name)

# Create path for TeraChem's input and output
tc_input = os.path.join(base_directory, "qmmm.sp")
tc_output = os.path.join(base_directory, "qmmm.out")

# Create path for NAMD's future input
namd_input = os.path.join(base_directory, f"{{input_name}}.result")

# =============================================================================
# Extract data from NAMD's output
# =============================================================================

# Open NAMD's output and extract the data
with open(input_name, "r") as namd_out:

        # Split the line by spaces
	temp = namd_out.readline().split()

	# Get the number of atoms
	num_atoms = int(temp[0])

	# Get the number of point charges
	numPntChr = int(temp[1])

	# Create a list to store the atomic coordinates
	xyz_lines = []

	# Create a list to store the point charges
	xyz_charges = []

	# For every *other* line in NAMD's outout ...
	for n_index, n_line in enumerate(namd_out):
		temp2 = n_line.split()

		# Get the coordinates
		posx, posy, posz = temp2[:3]

		if n_index < num_atoms:

			# Get the element
			element = temp2[3]

			# Add coordinates for the TeraChem input
			xyz_lines.append(f" {{element}} {{float(posx):>16.8f}} {{float(posy):>16.8f}} {{float(posz):>16.8f}}\n")

		else:
			# Get the atomic charge
			charge = temp2[3]

			# Add charges to the TeraChem input
			xyz_charges.append(f"{{charge}} {{posx:>16.8f}} {{posy:>16.8f}} {{posz:>16.8f}}\n")

	xyz_lines = [f"{{len(xyz_lines)}}\n", "XYZ coordinates for TeraChem\n"] + xyz_lines

with open("qmmm.xyz", "w") as coords:
	coords.writelines(xyz_lines)

with open("qmmm.q", "w") as charges:
	charges.writelines(xyz_charges)

# =============================================================================
# Create TeraChem's input
# =============================================================================
with open(tc_input, 'w') as tc_in:
	tc_in.writelines(tc_conf_line)

# =============================================================================
# Run TeraChem
# =============================================================================
os.chdir(base_directory)

# Open a file to store any standard output from TeraChem
with open(tc_output, "w") as tc_rt:
	log = subprocess.run(f"/home/bin/TeraChem/bin/terachem {{tc_input}}", shell=True, stdout=tc_rt)

# =============================================================================
# Extract data from TeraChem's output
# =============================================================================
with open(tc_output, 'r') as f:
	tc_out = f.readlines()

grad_line = -1

# Iterate over TeraChem's input
for q_index, q_line in enumerate(tc_out):

	# Extract the atom number according to TeraChem
	if "Total atoms:" in q_line:
		temp3 = q_line.split()
		qm_num_atoms = int(temp3[2])

		# Sanity check
		if qm_num_atoms != num_atoms:
			raise ValueError("The number of atoms from TeraChem is different from\
				the number of atoms from NAMD.")

	# Extract the energy
	if "FINAL ENERGY:" in q_line:
		temp4 = q_line.split()
		energy = float(temp4[2])

	# Locate the gradient
	if "Gradient" in q_line:
		grad_line = q_index + 3


# Reading the gradients only
qm_grads = {{'X':[0]*num_atoms, 'Y':[0]*num_atoms, 'Z':[0]*num_atoms}}
for index_grad in range(grad_line, grad_line + num_atoms):
	temp5 = tc_out[index_grad].split()
	temp6 = [float(t5) for t5 in temp5]
	qm_grads['X'][index_grad - grad_line] = float(temp6[0]) * -1185.82151
	qm_grads['Y'][index_grad - grad_line] = float(temp6[1]) * -1185.82151
	qm_grads['Z'][index_grad - grad_line] = float(temp6[2]) * -1185.82151

# Read the charges from the charge output file
with open("scr.qmmm/charge_mull.xls", "r") as q_file:
	all_charges = q_file.readlines()

# Reading the charges only
qm_charges = [0]*num_atoms
for index_charge in range(num_atoms):
	temp7 = all_charges[index_charge].split()
	qm_charges[index_charge] = float(temp7[2])

# =============================================================================
# Create NAMS's input
# =============================================================================
namd_in_lines = [f"{{energy * 627.509469}}\n"]
for item in range(num_atoms):
	namd_in_lines.append(f"{{qm_grads['X'][item]}} {{qm_grads['Y'][item]}} {{qm_grads['Z'][item]}} {{qm_charges[item]}}\n")

with open(namd_input, 'w') as namd_in:
	namd_in.writelines(namd_in_lines)
