#!/usr/bin/env python3

import os
import sys
import platform

# Get the name of NAMD's output, as this script's input
input_name = sys.argv[1]

# Get the number of processors
with open(input_name, "r") as in_f:
	data = in_f.readlines()

for l in data:
	if "nprocs" in l:
		temp = l.split()
		procs = int(temp[2])
		break

# Get the file's name
base_name = os.path.basename(input_name)
split_name = base_name.split(".")

# Get the file's path
base_directory = os.path.dirname(input_name)

# Create path for the nodes file
nodes_file = os.path.join(base_directory, f"{split_name[0]}.nodes")

# Get the name of this node
node = platform.node()
#node = node.split(".")[0]

with open(nodes_file, "w") as f:
	f.write(f"{node} slots={nprocs}\n")
