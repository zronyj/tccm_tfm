const app = Vue.createApp({
	data() {
		return {
			stickyNav: false,
			molloaded: false,
			qmSet: false,
			qmEnter: false,
			mdSet: false,
			mdEnter: false,
			xyzContent: "",
			qmProps: {
				software: ["Orca", "QUICK", "TeraChem"],
				functionals: ["PBE", "PW91", "BLYP", "B3LYP", "PBE0"],
				basis : ["3-21G", "6-31G", "6-311G",
						"6-31G*", "6-311G*",
						"6-31+G*", "6-311+G*",
						"6-31G**", "6-311G**",
						"6-31+G**", "6-311+G**",
						"6-31++G**", "6-311++G**",
						"def2-SVP", "def2-SVPD",
						"cc-pVDZ"]
			},
			qmTheory: "",
			qmBasis: "",
			qmSoft: "",
			mdTStep: 0.5,
			mdSteps: 100,
			mdTrajs: 500
		}
	},
	methods: {
		lockQM: function (ev) {
			ev.preventDefault();
			ev.stopPropagation();
			if ((this.qmTheory != "") && (this.qmBasis != "") && (this.qmSoft != "")) {
				this.qmEnter = false;
				this.qmSet = true;
			} else {
				this.qmEnter = true;
			}
			
		},
		lockMD: function (ev) {
			ev.preventDefault();
			ev.stopPropagation();
			if ((this.mdTStep != "") && (this.mdSteps != "") && (this.mdTrajs != "")) {
				this.mdEnter = false;
				this.mdSet = true;
			} else {
				this.mdEnter = true;
			}
			
		},
		loadMOL: function (content) {
			var lines = content.split("\n");
			this.xyzContent += lines[0] + "\n";
			this.xyzContent += lines[1] + "\n";
			for (var i = 2; i < lines.length; i++) {
				var temp = lines[i].split(/(\s+)/).filter( e => e.trim().length > 0);
				for (var j = 0; j < temp.length; j++) {
					if (j != 0) {
						var temp2 = temp[j].split(".");
						var spaces = " ".repeat( 7 - temp2[0].length );
						this.xyzContent += spaces + Number(temp[j]).toFixed(4);
					} else {
						this.xyzContent += temp[0];
					}
				}
				this.xyzContent += "\n";
			}
			//this.xyzContent = content;
			this.molloaded = true;
		},
		loadXYZ: function (ev) {
			ev.preventDefault();
			ev.stopPropagation();
			this.molloaded = false;
			this.xyzContent = "";
			var file = ev.target.files || ev.dataTransfer.files;
			if (!file.length)
				return;
			var reader = new FileReader();
			reader.readAsText(file[0], "UTF-8");
			var self = this;
			reader.onload = function (evt) {
				self.loadMOL(evt.target.result);
			};
			reader.onerror = function (evt) {
				console.log("Error loading molecule!");
			};
		}
	},
	mounted() {
		let plcholder = this.$refs.placeholder;
		window.document.onscroll = () => {
			let navBar = document.getElementById('nav');
			if (window.scrollY > navBar.offsetTop) {
				this.stickyNav = true;
			} else {
				this.stickyNav = false;
			}
		}
	}
});

app.component('viewer-3d', {
	props: ['xyzfile'],
	methods: {
		renderViewer: function (xyzData) {
			// Create the whole viewer
			let molViewer = new ChemDoodle.TransformCanvas3D('molViewer', 600, 600);
			molViewer.styles.set3DRepresentation('Ball and Stick');
			molViewer.styles.backgroundColor = 'black';
			let molecule = ChemDoodle.readXYZ(xyzData);
			molViewer.loadMolecule(molecule);
		}
	},
	mounted () {
		setTimeout(() => {
			this.renderViewer(this.xyzfile);
		}, 500);
	},
	template: `<canvas id='molViewer'></canvas>
	<p>If you see your molecule in the box, and you are sure to proceed, please proceed to <b>Step 2</b>.</p>`});

app.mount('#app');