#!/bin/bash

#SBATCH --account=zronyj_serv
#SBATCH --job-name=ethio
#SBATCH --partition=cccmd
#SBATCH --qos=cccmd
#SBATCH --time=00:30:00
#SBATCH --output=traso_snamd.log

#SBATCH --nodes=1              # Number of nodes to be used
#SBATCH --ntasks=1             # Number of instances to be run of the same program
#SBATCH --mincpus=16           # Minimum number of CPUs to get from Slurm
#SBATCH --cpus-per-task=16     # Number of processors assigned to each instance of the program
#SBATCH --threads-per-core=1   # Avoid hyperthreading

tmp=/dev/shm/0000
spo=spoils
export RSH_COMMAND=/usr/bin/ssh

module load python/3.10.0
module load gcc/11.2.0
module load autoload openmpi/gnu/4.1.1_gnu_11.2.0
module load orca/5.0.3

# Creating nodefile in scratch
echo $SLURM_NODELIST > $SLURM_JOB_NAME.nodes
echo $SLURM_NTASKS
echo $SLURM_CPUS_ON_NODE
echo $SLURM_CPUS_PER_TASK

if [ -d "$tmp" ]; then
    rm -R "$tmp"
fi
mkdir $tmp

/lustre/home/zronyj/tccm_tfm/Scripts/Launchers_CCC/cccmd/NAMD2_14/namd2 +p1 Trajectory_0000/ethio.namd +setcpuaffinity +showcpuaffinity > Trajectory_0000/ethio.log

if [ -d "$spo" ]; then
    rm -R "$spo"
fi
mkdir spoils

cp -R /dev/shm/0000/* spoils
rm -R "$tmp"
