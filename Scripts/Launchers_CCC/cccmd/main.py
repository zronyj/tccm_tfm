#!/usr/bin/env python3

# ------------------ Importing libraries ------------------
import os
import shutil
from sys import argv
from time import time
import numpy as np
from multiprocessing import Pool
from scipy import constants as cts
from file_io import orca, namd
from physics import sampling

# ------------------ Inputs and locations -----------------

# Get the current location
here = os.getcwd()

# Get this program's location
prog = os.path.dirname(os.path.abspath(__file__))

qm_options = {}
# Orca <-----------------------------------|
# https://sites.google.com/site/orcainputlibrary/general-input
qm_options["ORCA"] = {
    "Name" : "ORCA",
    "Level" : "DFT",
    "Theory" : "B3LYP D3BJ",
    "Basis set" : "6-31G*",
    "Cores" : 4,
    "Memory" : 1536,
    "SCF" : 100,
    "Path" : "/usr/local/orca/5.0.3/orca",
    "Template" : "o_template.namd"
}

# QUICK <-----------------------------------|
# https://quick-docs.readthedocs.io/en/latest/user-manual.html#keywords-for-quick-input
qm_options["QUICK"] = {
    "Name" : "QUICK",
    "Level" : "DFT",
    "Theory" : "B3LYP D3BJ",
    "Basis set" : "6-31G*",
    "SCF" : 500,
    "Path" : os.path.join(prog, "wrappers", "quick_wrapper.py"),
    "Template" : "q_template.namd"
}

# TeraChem <-----------------------------------|
# http://www.petachem.com/doc/userguide.pdf
qm_options["TeraChem"] = {
    "Name" : "TeraChem",
    "Level" : "DFT",
    "Theory" : "B3LYP",
    "Basis set" : "6-31gs",
    "SCF" : 500,
    "Path" : os.path.join(prog, "wrappers", "tc_wrapper.py"),
    "Template" : "t_template.namd"
}

# ----------------------- Functions -----------------------

def compute_frequencies(xyz_file, qmpath, method, basis, cores, memory):

    # Take the starting XYZ file, and perform an optimization
    # and frequencies calculation
    if not(os.path.exists(f"{xyz_file[:-3]}out")):
        orca.make_run_input(xyz_file, qmpath, method=method,
        basis=basis, opt="Opt", cores=cores, memory=memory)

    # Get all the information from the ORCA output file
    na, mass, masses, coords, nm, freqs = orca.parse_output(
        f"{xyz_file[:-4]}.out"
        )

    print(f"Number of atoms: {na}")
    print(f"Molecule mass: {mass:8.2f}")

    # Compute the ZPE of the molecule
    zpes = sampling.get_ZPEs(freqs)
    zpe = sum(zpes)

    print(
        (f"The Zero Point Energy for this molecule is {zpe:.4E} J "
         f"or {zpe * cts.Avogadro / (1000 * cts.calorie):.4F} kcal/mol.")
        )

    return na, mass, masses, coords, nm, freqs

def compute_trajectories(tgt_energy, trajs, masses, coords, nm, freqs, samp):

    # Setting the random numbers for deterministic results
    random_seed = np.random.randint(1E6, 1E7)
    print(f"Random seed for this run is: {random_seed}")
    np.random.seed(random_seed)

    # Rescale the target energy to J/mol
    Etot = tgt_energy * (1000 * cts.calorie) / cts.Avogadro

    print(
        (f"According to the provided energy, the new system energy "
         f"should be around {Etot:.4E} J. or {tgt_energy:.4F} kcal/mol.")
        )

    # Generate random numbers for the trajectories
    seed_field = np.random.randint(1000000, 9999999, size=trajs)

    # Getting variables ready for all the data
    new_coords = [0]*trajs
    vels = [0]*trajs
    new_Etot = [0]*trajs

    # Select the sampling technique
    # Microcanonical sampling
    if samp == 'microcan':
        # Set all levels to 0
        lvls = [[0]*len(nm)]*trajs

    # Levels sampling
    elif samp == 'levels':

        # Read the levels from a file
        with open('levels.dat', 'r') as lvls_file:
            lvls_lines = lvls_file.readlines()

        # Check if there are enough lines to match trajectories
        if len(lvls_lines) != trajs:
            raise ValueError(
                ("The number of levels trajectories is not the same as "
                "the number of requested trajectories.")
                )

        # Check if there are enough levels to match the normal modes
        lvls_lines = [l.split() for l in lvls_lines]
        if len(lvls_lines[0]) != len(nm):
            raise ValueError(
                ("The number of provided levels is not the same as "
                "the number of normal modes.")
                )

        # Build the levels array
        lvls = [[int(i) for i in l] for l in lvls_lines]

    # Doing the process over all available cores
    with Pool() as p:
        # Put all arguments in the same item for each set of random numbers
        items = [
        (coords, masses, freqs, nm, Etot, samp, lvls[i], rn) \
        for i, rn in enumerate(seed_field)
        ]
        # Compute the coordinates, velocity vectors and new energies
        everything = p.starmap(sampling.get_initial_conditions, items)

    # Isolating terms ...
    new_coords, vels, new_Enm, new_Etot, n_modes = zip(*everything)

    # Preparing data for detailed output
    title = "Traj     Seed "
    title += "  ".join([f"  Mode_{i+1:>03d}" for i in range(len(n_modes[0]))])
    title += "     Est_Energ_(J)  Scaled_Energ_(J)  Scaled_Energ_(kcal/mol)\n"
    exc_data = [title]
    for t in range(trajs):
        modes = [f"{m:>10d}" for m in n_modes[t]]
        exc_data.append(f"{t:04d}  {seed_field[t]} " +\
        "  ".join(modes) + f"  {new_Enm[t]:16.8e}  {new_Etot[t]:16.8e}" +\
        f"  {new_Etot[t]* cts.Avogadro/(1000*cts.calorie):>23.4f}\n")

    # Save filed with details on the trajectories
    with open("microcanonical_modes.csv", "w") as mccm:
        mccm.writelines(exc_data)

    print(
        (f"But the average system energy will be around "
         f"{sum(new_Etot)/trajs:.4E} J. or "
         f"{sum(new_Etot)/trajs * cts.Avogadro/(1000*cts.calorie):.4f}"
         " kcal/mol.")
        )

    return new_coords, vels, new_Etot

def prepare_trajectories(trajs, steps, ts, na, qm_pack, sim_name,
    p_directory, e_directory, new_coords, vels):

    print(f"The time of the simulation will be of: {steps * ts/1000} ps")

    # Set the path for all the CHARMM parameter files
    parameter_dir = os.path.join(p_directory, "toppar")

    # Selecting program for QM/MM
    namd_template_name = qm_pack["Template"]

    if qm_pack["Name"] == "ORCA":
        if qm_pack["Level"] != "SE":
            orca_MPI = f'''qmConfigLine "%%maxcore {qm_pack["Memory"]}"
qmConfigLine "%%pal nprocs {qm_pack["Cores"]} end"
qmConfigLine "%%scf\nmaxiter {qm_pack["SCF"]}\nTolE 3e-5\nConvCheckMode 1\nend"'''
        else:
            orca_MPI = ""

    elif qm_pack["Name"] == "QUICK":
        # Produce a new QUICK wrapper with the necessary inputs
        w_temp = os.path.join(prog, "templates", "quick_wrapper_template.py")
        with open(w_temp, "r") as q_w_temp:
            quick_template = q_w_temp.read()

        # Write the new QUICK wrapper
        wrapper_fin = quick_template.format(lev=qm_pack["Level"],
            theo=qm_pack["Theory"], bset=qm_pack["Basis set"],
            scf=qm_pack["SCF"])
        with open(qm_pack["Path"], "w") as q_wrapper:
            q_wrapper.write(wrapper_fin)

    elif qm_pack["Name"] == "TeraChem":
        # Produce a new TeraChem wrapper with the necessary inputs
        w_temp = os.path.join(prog, "templates", "tc_wrapper_template.py")
        with open(w_temp, "r") as tc_w_temp:
            tc_template = tc_w_temp.read()

        # Write the new TeraChem wrapper
        wrapper_fin = tc_template.format(theo=qm_pack["Theory"],
            bset=qm_pack["Basis set"], scf=qm_pack["SCF"])
        with open(qm_pack["Path"], "w") as q_wrapper:
            q_wrapper.write(wrapper_fin)

    # Get the data structure for future PDB files
    pdb_template = namd.get_pdb_data(sim_name)

    # Create the NAMD configuration file to run the simulation
    # Use EnGrad: https://www.ks.uiuc.edu/Research/namd/mailing_list/namd-l.2016-2017/1376.html
    # Where is the temp file? /dev/shm - https://www.cyberciti.biz/tips/what-is-devshm-and-its-practical-usage.html
    template_file = os.path.join(prog, "templates", namd_template_name)
    with open(template_file, 'r') as namd_conf_template:
        template = namd_conf_template.read()

    # Create all the input files for all trajectories in separate folders
    for traj in range(trajs):

        # Establish new trajectory's path
        new_dir = os.path.join(here, f"Trajectory_{traj:04d}")

        # If the directory exists, delete it
        if os.path.exists(new_dir):
            shutil.rmtree(new_dir)

        # Create the directory
        os.mkdir(new_dir)

        # Go into the new folder
        os.chdir(new_dir)

        # Temporary RAM folder
        tmppath = os.path.join("/dev/shm", f"{traj:04d}")
        if not(os.path.exists(tmppath)):
            os.mkdir(tmppath)

        # Save the XYZ coordinates
        # https://open-babel.readthedocs.io/en/latest/FileFormats/XYZ_cartesian_coordinates_format.html
        lines = []
        lines.append(f"{na}\n")
        lines.append("Molecule generated with the Trajectory Software\n")
        pretty_format = " {}\t{:>18.15f}\t{:>18.15f}\t{:>18.15f}\n"
        lines += [pretty_format.format(*a) for a in new_coords[traj]]

        with open(f"{sim_name}.xyz", "w") as xyz:
            xyz.writelines(lines)

        # Create the PDB file with the new coordinates for this run
        temp_coords_only = [ atom[1:] for atom in new_coords[traj] ]
        temp_coords = []
        for tco in temp_coords_only:
            temp_coords += tco

        with open(f"{sim_name}.pdb", "w") as pdb:
            pdb.write(pdb_template.format(*temp_coords))

        # Create a topology file with "something"
        namd.make_rtf(new_coords[traj])

        # Create the new PDB with the initial coordinates for the MD
        namd.make_pdb(na, f"{sim_name}_proc.pdb",
            f"{sim_name}.pdb", new_coords[traj])

        # Create a PSF file based on the latter
        namd.make_psf(na, new_coords[traj], f"{sim_name}_proc.psf")

        # Slurm modification!!!! -------------------------------
        pre_path = os.path.join(prog, "templates", "o_prerun.py")

        # Fill in NAMD template
        if qm_pack["Name"] == "ORCA":
            namd_file_content = template.format(name=sim_name,
            tmpfolder=tmppath, steps=steps, ts=ts, qmpath=qm_pack["Path"],
            theo=qm_pack["Theory"], bset=qm_pack["Basis set"], mpi=orca_MPI,
            pardir=parameter_dir, prepath=pre_path)
        elif qm_pack["Name"] == "QUICK":
            namd_file_content = template.format(name=sim_name,
            tmpfolder=tmppath, steps=steps, ts=ts, qmpath=qm_pack["Path"],
            pardir=parameter_dir)
        elif qm_pack["Name"] == "TeraChem":
            namd_file_content = template.format(name=sim_name,
            tmpfolder=tmppath, steps=steps, ts=ts, qmpath=qm_pack["Path"],
            pardir=parameter_dir)

        with open(f"{sim_name}.namd", "w") as namd_conf:
            namd_conf.write(namd_file_content)

        # Create the new velocities file
        namd.make_pdb(na, f"{sim_name}_proc.vel",
            f"{sim_name}.pdb", vels[traj])

        # Get out of the folder
        os.chdir(here)

def start_qmmm(sim_name, trajs, cwd, namd_path, t_cores, md_cores):
    # Running the QM/MD simulation
    with Pool(t_cores) as p:

        # Compute the name of all the folders
        traj_args = []
        for t in range(trajs):
            temp_path = os.path.join(
                os.path.join(cwd, f"Trajectory_{t:04d}"),
                sim_name
                )
            traj_args.append((temp_path, namd_path, str(md_cores)))

        # Run QM/MD with NAMD
        jobs = p.starmap(namd.run_NAMD, traj_args)

def restart_qmmm(sim_name, cwd, namd_path, t_cores, md_cores):
    # Get all the trajectories
    folders = [f for f in os.listdir(cwd) if "Trajectory_" in f]

    # If there's no Trajectory folders ... ?
    if len(folders) == 0:
        raise FileNotFoundError(
            ("There are no trajectory folders "
            "at this location!")
        )

    # Check which folders hold a .log file
    # If there is one, TraSo will NOT use that folder to run calculations
    remaining = []
    for f in folders:
        if not os.path.exists(os.path.join(cwd, f, f"{sim_name}.log")):
            remaining.append(f)

    # If there is no Trajectory_ folder without a .log file in them ...?
    if len(remaining) == 0:
        raise FileNotFoundError(
            ("All Trajectory_ folders have .log files in them. "
            "Therefore, no simulation will be started.")
        )

    restart_i_time = time()

    # Running the QM/MD simulation
    with Pool(t_cores) as p:

        # Compute the name of all the folders
        traj_args = []
        for f in remaining:
            temp_path = os.path.join(
                os.path.join(cwd, f),
                sim_name
                )
            traj_args.append((temp_path, namd_path, str(md_cores)))

        # Run QM/MD with NAMD
        jobs = p.starmap(namd.run_NAMD, traj_args)

    restart_f_time = time()
    print(
        ("Time to run all remaining trajectories: "
        f"{restart_f_time - restart_i_time:10.4f} s.")
        )

# ---------------------------------------------------------
# ----------------- Initialize  variables -----------------
# ---------------------------------------------------------

# Name of the XYZ file used as starting point
if len(argv) != 2:
    raise ValueError("No XYZ file provided!")
if argv[1][-3:] != "xyz":
    raise ValueError("The provided file is not an XYZ file!")

initial_xyz_file = argv[1]

# The name of the files
simulation_name = initial_xyz_file[:-4]

# Set the target energy of the molecule in kcal/mol
target_energy = 320

# Set the number of trajectories
trajectories = 100

# Steps in the trajectory
steps = 400

# Simulation time step in femtoseconds (fs)
time_step = 0.5

# Number of trajectories to propagate simultaneously
trajectory_cores = 2

# NAMD cores to be used per trajectory
NAMD_cores = 1

# NAMD path
#namd_path = "/opt/NAMD2mc/namd2"
#namd_path = "/home/rony/Software/NAMD_2.14/namd2"
#namd_path = "/usr/local/namd/2.11b1/namd2"
namd_path = f"{prog}/NAMD2_14/namd2"

# QM software for the QM/MM: "ORCA", "QUICK" or "TeraChem"
qm_soft = qm_options["ORCA"]

# ------------------------ Program ------------------------

if __name__ == "__main__":

    # Time when the simulation starts
    i_time = time()

    # Optimize the molecule and compute its frequencies (Hessian)
    na, mass, masses, coords, nm, freqs = compute_frequencies(
        initial_xyz_file, qm_options["ORCA"]["Path"], "B3LYP",
        "6-31G*", 8, 1536)

    q_time = time()
    print(f"Time for optimization and Hessian: {q_time - i_time:10.4f} s.")

    # ===========================================================

    # Use the normal modes to compute the new trajectories
    new_coords, vels, new_Etot = compute_trajectories(
        target_energy, trajectories, masses, coords, nm, freqs, 'microcan')

    nm_time = time()
    print(f"Time to compute trajectories: {nm_time - q_time:10.4f} s.")

    # ===========================================================

    # Make all the folders for the trajectories
    prepare_trajectories(trajectories, steps, time_step, na, qm_soft,
        simulation_name, prog, here, new_coords, vels)

    t_time = time()
    print(f"Time to create trajectory files: {t_time - nm_time:10.4f} s.")

    # ===========================================================
    quit()
    # Propagate the trajectories
    start_qmmm(simulation_name, trajectories, here, namd_path,
        trajectory_cores, NAMD_cores)

    f_time = time()
    print(f"Time to run all trajectories: {f_time - t_time:10.4f} s.")
    print(f"Total elapsed time: {f_time - i_time:10.4f} s.")
