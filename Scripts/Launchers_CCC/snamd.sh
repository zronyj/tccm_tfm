#!/bin/bash

#SBATCH --account=zronyj_serv
#SBATCH --partition=cccmd
#SBATCH --qos=cccmd
#SBATCH --output=traso_snamd.log

#SBATCH --nodes=1              # Number of nodes to be used
#SBATCH --mincpus=16           # Number of processors
#SBATCH --ntasks=1             # Number of instances to be run of the same program
#SBATCH --cpus-per-task=16     # Number of processors assigned to each instance of the program
#SBATCH --threads-per-core=1   # Avoid hyperthreading

module load python/3.10.0
module load orca/5.0.3
module load namd/2.11b1

# Creating nodefile in scratch
echo $SLURM_NODELIST > enabler.nodes
echo $SLURM_NTASKS
echo $SLURM_CPUS_ON_NODE
echo $SLURM_CPUS_PER_TASK

srun 
