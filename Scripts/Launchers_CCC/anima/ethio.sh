#!/bin/bash

#SBATCH --account=moleculas_serv
#SBATCH --partition=anima
#SBATCH --qos=abigp
#SBATCH --job-name=test_traso
#SBATCH --output=test_traso.log

## https://stackoverflow.com/questions/65456729/problems-with-orca-and-openmpi-for-parallel-jobs
#SBATCH --mincpus=12           # Number of processors
#SBATCH --ntasks=1             # Number of instances to be run of the same program
#SBATCH --cpus-per-task=12     # Number of processors assigned to each instance of the program
#SBATCH --threads-per-core=1   # Avoid hyperthreading

module load python/3.10.0
module load openbabel/3.1.1
module load orca/5.0.3
module load namd/2.11b1

traso="/home/zronyj/tccm_tfm/Scripts/TraSo/TraSo.tar.gz"
namd2="/home/zronyj/NAMD.tar.gz"
basef="/home/zronyj/tccm_tfm/Molecules/CCC/ethio"
sbcast $traso TraSo.tar.gz
sbcast $namd2 NAMD.tar.gz
sbcast $basef/ethio.xyz ethio.xyz
sbcast $basef/ethio.hess ethio.hess
sbcast $basef/ethio.inp ethio.inp
sbcast $basef/ethio.opt ethio.opt
sbcast $basef/ethio.out ethio.out

srun tar -xzf NAMD.tar.gz
srun tar -xzf TraSo.tar.gz
srun rm TraSo.tar.gz
srun rm NAMD.tar.gz
srun main.py ethio.xyz
srun mkdir spoils
srun cp -R /dev/shm/0000/* ./spoils